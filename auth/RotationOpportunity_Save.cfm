
<cfparam name="FORM.directorsCAPHR_ID" default="00000000">
<cfparam name="FORM.rotationOppStatus" default="published">
<cfparam name="FORM.devOpp1" default="0" />
<cfparam name="FORM.devOpp2" default="0" />
<cfparam name="FORM.devOpp3" default="0" />
<cfparam name="FORM.devOpp4" default="0" />
<cfparam name="FORM.devOpp5" default="0" />
<cfparam name="FORM.devOpp6" default="0" />
<cfparam name="FORM.devOpp7" default="0" />
<cfparam name="FORM.devOpp8" default="0" />
<cfparam name="FORM.devOpp9" default="0" />
<cfparam name="FORM.devOpp10" default="0" />
<cfparam name="FORM.devOpp11" default="0" />
<cfparam name="FORM.devOpp12" default="0" />
<cfparam name="FORM.ECQ_LeadingChange" default="0" />
<cfparam name="FORM.ECQ_LeadingPeople" default="0" />
<cfparam name="FORM.ECQ_ResultsDriven" default="0" />
<cfparam name="FORM.ECQ_BusinessAcumen" default="0" />
<cfparam name="FORM.ECQ_BuildingCoalitions" default="0" />
<cfparam name="FORM.ECQ_HowOppRelates" default="0" />
<cfparam name="FORM.subComponent" default="" />

<cfif structKeyExists(FORM, "btnSaveRotation")>
    <cfdump var="#FORM#" label="FORM" expand="true" abort="false"/>
</cfif>

<cftry>
    <!---<cfset var ins_RotationOpportunity = "">--->
    <cfquery name="ins_RotationOpportunity" >
	INSERT INTO RotationOpp (
    centerOffice
    ,subComponent
    ,justification
    ,rotationTitle
    ,clearencesRequired
    ,numPositions
    ,officeAddress
    ,execSponsor
    ,hostSupervisorCapHR_ID
    ,hostSupervisorName
    ,hostSupervisorTitle
    ,hostSupervisorEmail
    ,hostSupervisorPhone
    ,hostSupervisorSignature
    ,hostSupervisorSignDate
    ,agencyPOC
    ,workplaceFlexibilities
    ,newRotationHost
    ,opportunityDescription
    ,ECQ_LeadingChange
    ,ECQ_LeadingPeople
    ,ECQ_ResultsDriven
    ,ECQ_BusinessAcumen
    ,ECQ_BuildingCoalitions
    ,ECQ_HowOppRelates
    ,devOpp1
    ,devOpp2
    ,devOpp3
    ,devOpp4
    ,devOpp5
    ,devOpp6
    ,devOpp7
    ,devOpp8
    ,devOpp9
    ,devOpp10
    ,devOpp11
    ,devOpp12
    ,otherDevOpp
    ,howBenefitParticipant
    ,specialRequirements
    ,dateEntered
   )
        values (
    <cfqueryparam value="#FORM.centerOffice#" 								cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.subComponent#" 							cfsqltype="cf_sql_varchar" null="#NOT(trim(len(FORM.subComponent)))#">
    ,<cfqueryparam value="#FORM.justification#" 						cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.rotationTitle#" 						cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.clearencesRequired#" 					cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.numPositions#" 							cfsqltype="cf_sql_integer">
    ,<cfqueryparam value="#FORM.officeAddress#" 						cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.execSponsor#" 							cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorCapHRID#" 				cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorName#" 					cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorTitle#" 					cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorEmail#" 					cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorPhone#" 					cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.hostSupervisorSignature#"               cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#listFirst(FORM.hostSupervisorSignDate)#" 	cfsqltype="cf_sql_date">
    ,<cfqueryparam value="#FORM.agencyPOC#" 							cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.workplaceFlexibilities#" 				cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.newRotationHost#" null="#NOT(trim(len(FORM.newRotationHost)))#" cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.opportunityDescription#" cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.ECQ_LeadingChange#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.ECQ_LeadingPeople#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.ECQ_ResultsDriven#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.ECQ_BusinessAcumen#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.ECQ_BuildingCoalitions#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.ECQ_HowOppRelates#" cfsqltype="cf_sql_varchar">
    ,<cfqueryparam value="#FORM.devOpp1#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp2#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp3#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp4#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp5#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp6#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp7#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp8#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp9#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp10#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp11#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.devOpp12#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.otherDevOpp#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.howBenefitParticipant#" cfsqltype="varchar">
    ,<cfqueryparam value="#FORM.specialRequirements#" cfsqltype="varchar">
    ,<cfqueryparam value="#DateFormat(FORM.dateEntered,'yyyy-mm-dd')#" cfsqltype="cf_sql_date">
    )
</cfquery>
    <cfcatch type="database">
        <cfif Application.environment IS NOT "Development" and Application.environment IS NOT "SharedDev">
            <cferror type="exception" exception="any" template="error.cfm">
            <cfelse>
            <cfdump var="#cfcatch#" label="catch block" abort="true">
        </cfif>
    </cfcatch>
</cftry>
<cflocation url="Summary.cfm" />