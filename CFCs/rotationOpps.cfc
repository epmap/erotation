<cfcomponent displayname="rotationOpps" hint="ColdFusion Component Rotation Opportunity management">

	<cfset VARIABLES.instance = {
		datasource = ''
	}/>

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="datasource" required="true" type="CFC.Datasource"/>

		<cfscript>
			VARIABLES.instance.datasource = arguments.datasource;
		</cfscript>
		<cfreturn this/>
	</cffunction>

	<cffunction name="fnc_summaryActiveRotationOpps" hint="I get just the active rotation opportunity record fields for summary" returntype="query" output="false" access="public">
        <cfargument name="isActive" required="false" type="String" default="1"/>
		<cfargument name="center_ID" type="numeric" default="0"/>
        <cftry>
			<cfset variables.qsummaryActiveRotationOpps = ''>
			<cfquery name="qsummaryActiveRotationOpps">
                SELECT RO.rotOpp_ID,
                hostSupervisorName,
                RO.hostSupervisorCapHR_ID,
                RO.centerOffice,
                justification,
                clearencesRequired,
                RO.subComponent,
                RO.rotationTitle,
                opportunityDescription,
                participantLocation
                FROM RotationOpp RO
                WHERE 1 = 1
			<cfif structKeyExists(ARGUMENTS, "isActive") AND len(arguments.isActive)>
                AND RO.isActive = <cfqueryparam  value="#arguments.isActive#" cfsqltype="cf_sql_varchar">
			</cfif>
			<cfif structKeyExists(ARGUMENTS, "center_ID") AND len(arguments.center_ID) and arguments.center_ID GT 0>
                    AND RO.centerOffice = <cfqueryparam value="#arguments.center_ID#" cfsqltype="cf_sql_integer">
			</cfif>
		</cfquery>
        <cfcatch>
<!---todo: replace with real error handling--->
				<cfif application.environment IS NOT "Development" AND application.environment IS NOT "SharedDev">
					<cferror type="exception" exception="any" template="error.cfm">
					<cfelse>
					<cfdump var="#cfcatch#" label="qryActiveRotationOpps" expand="true" abort="true"/>
				</cfif>
        </cfcatch>
        </cftry>
		<cfreturn qsummaryActiveRotationOpps />
	</cffunction>

	<cffunction name="fnc_RotationOppsBySupervisor" hint="I get the active rotation opportunities enteredy by on Host Supervisor" returntype="query" output="false" access="public">
		<cfargument name="isActive" required="false" type="String" default=""/>
		<cfargument name="hostSupervisorCapHR_ID" required="false" type="string" default=""/>
		<cfargument name="center_ID" type="numeric" default="0"/>
		<cftry>
			<cfset variables.qRotationOppsBySupervisor = ''>
			<cfquery name="qRotationOppsBySupervisor">
                SELECT RO.rotOpp_ID,
                hostSupervisorName,
                RO.hostSupervisorCapHR_ID,
                RO.centerOffice,
                justification,
                clearencesRequired,
                RO.subComponent,
                RO.rotationTitle,
                opportunityDescription,
                participantLocation,
                RR.participantCapHR_ID,
				RR.dtLastAction
                FROM RotationOpp RO
                LEFT OUTER JOIN RotationRequests RR
                on RO.rotOpp_ID = RR.rotOpp_ID
                WHERE 1 = 1
				<cfif structKeyExists(ARGUMENTS, "isActive") AND len(arguments.isActive)>
                    AND RO.isActive = <cfqueryparam  value="#arguments.isActive#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif structKeyExists(ARGUMENTS, "hostSupervisorCapHR_ID") AND len(arguments.hostSupervisorCapHR_ID)>
                    AND RO.hostSupervisorCapHR_ID = <cfqueryparam  value="#arguments.hostSupervisorCapHR_ID#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif structKeyExists(ARGUMENTS, "participantCapHR_ID") AND len(arguments.participantCapHR_ID)>
                    AND RR.participantCapHR_ID = <cfqueryparam  value="#arguments.participantCapHR_ID#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif structKeyExists(ARGUMENTS, "center_ID") AND len(arguments.center_ID) and arguments.center_ID GT 0>
                    AND RO.centerOffice = <cfqueryparam value="#arguments.center_ID#" cfsqltype="cf_sql_integer">
				</cfif>
			</cfquery>
			<cfcatch>
<!---todo: replace with real error handling--->
				<cfdump var="#cfcatch#" label="qryActiveRotationOpps" expand="true" abort="true"/>
			</cfcatch>
		</cftry>
		<cfreturn qRotationOppsBySupervisor />
	</cffunction>

	<cffunction name="fnc_RotationOppsByParticipant" hint="I get the active rotation opportunities requested by one participant" returntype="query" output="false" access="public">
		<cfargument name="isActive" required="false" type="String" default=""/>
		<cfargument name="participantCapHR_ID" required="true" type="string" default=""/>
		<cfargument name="center_ID" type="numeric" default="0"/>
		<cftry>
			<cfset variables.qRotationOppsByParticipant = ''>
			<cfquery name="qRotationOppsByParticipant">
                SELECT RO.rotOpp_ID,
                hostSupervisorName,
                RO.hostSupervisorCapHR_ID,
                RO.centerOffice,
                justification,
                clearencesRequired,
                RO.subComponent,
                RO.rotationTitle,
                opportunityDescription,
                participantLocation,
                RR.participantCapHR_ID,
				RO.rotationOppStatus
				FROM RotationOpp RO
                JOIN RotationRequests RR
                on RO.rotOpp_ID = RR.rotOpp_ID
                WHERE 1 = 1
				<cfif structKeyExists(ARGUMENTS, "participantCapHR_ID") AND len(arguments.participantCapHR_ID)>
                    AND RR.participantCapHR_ID = <cfqueryparam  value="#arguments.participantCapHR_ID#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif structKeyExists(ARGUMENTS, "center_ID") AND len(arguments.center_ID) and arguments.center_ID GT 0>
                    AND RO.centerOffice = <cfqueryparam value="#arguments.center_ID#" cfsqltype="cf_sql_integer">
				</cfif>
			</cfquery>
			<cfcatch>
				<!---todo: replace with real error handling--->
				<cfdump var="#cfcatch#" label="qryActiveRotationOpps" expand="true" abort="true"/>
			</cfcatch>
		</cftry>
		<cfreturn qRotationOppsByParticipant />
	</cffunction>

</cfcomponent>