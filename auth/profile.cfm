<cf_header title="eRotation Employee Profile">

<cfif structKeyExists(url,'action') AND url.action IS "update" >

	<cfinvoke component="CFCs/employee" method="updateEmployeeProfile">
	<cflocation url="profile.cfm?EmployeeID=#URL.EmployeeID#&profileUpdated" addtoken="no">

</cfif>

	<cfinvoke component="CFCs/employee" method="qGetEmployeeData" UUID="#url.employeeID#" returnvariable="rtn_qGetEmployeeData">
	<cfinvoke component="CFCs/employee" method="getAppRoles" returnvariable="rtn_getAppRoles">

    <cfoutput>

		<div class="container-fluid" style="margin: 0px 25px 0px 25px">

			<div class="row">
				<h2><span style="font-weight: 400">Manage Employee Profile</span>:&nbsp;&nbsp; #rtn_qGetEmployeeData.First_Name# #rtn_qGetEmployeeData.Last_Name#</h2>
			</div>

			<cfif structKeyExists(url,'profileUpdated')>
				<cf_checkmark type="Info" message="Your changes were saved">
			</cfif>

			<form name="employeeProfileForm" method="post" action="profile.cfm?action=update&EmployeeID=#rtn_qGetEmployeeData.Employee_UUID#">

				<input type="hidden" name="App2RoleID" value="#rtn_qGetEmployeeData.EmpRole_ID#">
				<input type="hidden" name="empName" value="#rtn_qGetEmployeeData.First_Name# #rtn_qGetEmployeeData.Last_Name#">
				<input type="hidden" name="empID" value="#rtn_qGetEmployeeData.CAPHR_ID#">
				<input type="hidden" name="cur_employee_role" value="#rtn_qGetEmployeeData.Role_ID#">
				<input type="hidden" name="cur_employee_role_name" value="#rtn_qGetEmployeeData.Role_Name#">
				<input type="hidden" name="cur_accountStatus" value="#rtn_qGetEmployeeData.isActive#">

				<!--- Top Data --->
				<div class="row" style="margin-top: 25px;">

					<!--- Left Side Data --->
					<div class="col-sm-6">
						<div class="x_panel tile">
							<div class="x_title" style="border-bottom: solid 1px lightgray; margin-bottom: 10px;">
								<h3 style="color: black;"><i class="fa fa-user-o" style="margin-right: 5px"></i> Employee Information</h3>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Employee</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.First_Name# #rtn_qGetEmployeeData.Last_Name#</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Employee ID</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.CAPHR_ID#</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Title</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.Job_Title#</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Position</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.Occ_Series#, #rtn_qGetEmployeeData.Pay_Plan# #rtn_qGetEmployeeData.Grade#-#rtn_qGetEmployeeData.Step#</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Email</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.email_address#</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Phone</b>:</div>
							<div class="col-sm-10"><cfif LEN(rtn_qGetEmployeeData.phone)>#rtn_qGetEmployeeData.phone#<cfelse><i>No Phone Entered</i></cfif></div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Center/Office</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.Center_Name# (#rtn_qGetEmployeeData.Center_Acronym#)</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-2"><b>Sub-Office</b>:</div>
							<div class="col-sm-10">#rtn_qGetEmployeeData.office_name#</div>
						</div>
					</div>
					<div class="col-sm-1">&nbsp;</div>
					<!--- Right Side Data --->
					<div class="col-sm-5">
						<div class="x_panel tile">
							<div class="x_title" style="border-bottom: solid 1px lightgray; margin-bottom: 10px;">
								<h3 style="color: black;"><i class="fa fa-address-card-o" style="margin-right: 5px"></i> eRotation Access</h3>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3"><b>Employee Role</b>:</div>
							<div class="col-sm-6">
								<select name="employee_role" class="form-control" data-dropup-auto="false" required>
									<option value="">Select eRotation Role .....</option>
										<cfloop query="rtn_getAppRoles">
										<option value="#rtn_getAppRoles.role_id#" <cfif rtn_qGetEmployeeData.Role_ID EQ rtn_getAppRoles.role_id>selected</cfif>>#rtn_getAppRoles.Role_Name#</option>
										</cfloop>
								</select>
							</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3"><b>Account Status</b>:</div>
							<div class="col-sm-6">
								<select name="accountStatus" class="form-control" data-dropup-auto="false" required>
									<option value="" <cfif NOT LEN(rtn_qGetEmployeeData.isActive)>selected</cfif>>Select Status .....</option>
									<option value="1" <cfif rtn_qGetEmployeeData.isActive EQ 1>SELECTED</cfif>>Active </option>
									<option value="0" <cfif rtn_qGetEmployeeData.isActive EQ 0>SELECTED </cfif>>Disabled </option>
								</select>
							</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-3">&nbsp;</div>
						</div>
						<div class="row" style="margin: 5px 0 5px 0">
							<div class="col-sm-12 text-right">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-pencil-square-o" title="Update" style="margin-right: 3px"></i> Update Profile Information
								</button>
							</div>
						</div>
					</div>

				</div>

			</form>
			<hr style="margin: 10px">

				<div class="col-sm-12 text-right">
					<a href="#application.baseURL#/tools/manage_users.cfm#session.queryString#">
						<button class="btn btn-info" style="padding: 5px">
						<i class="fa fa-users" title="Back to Manage Users" style="margin-right: 3px"></i> Back to Manage Users
						</button>
					</a>
				</div>
		<div class="row"></div>
		</div>

    </cfoutput>

<cf_footer>