
		</div> <!-- boxed -->
	</div><!-- sb-site -->


<div style="min-height: 100px"></div>

<!-- footer-widgets -->

<footer id="footer">
	<cfoutput>
		<p>&copy; #dateFormat(now(), "YYYY")# <a href="http://www.gapsi.com/">GAP Solutions</a>, inc. All rights reserved.</p>
		<cfif structKeyExists(application,'ReleaseVersion')>
			<p>ePMAP #application.ReleaseVersion# - #application.Environment#</p>
		</cfif>
	</cfoutput>
</footer>

	</body>
</html>
