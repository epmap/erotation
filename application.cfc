<cfcomponent
		displayname="APPLICATION"
		output="true"
		hint="Handle the APPLICATION.">

<!--- Set up the APPLICATION. --->
	<cfset VARIABLES.AppName = 'eRotation'>
	<cfset THIS.Name = VARIABLES.AppName/>
	<cfset THIS.ApplicationTimeout = CreateTimeSpan(0, 0, 30, 0)>
	<cfset THIS.SESSIONManagement = true>
	<cfset THIS.SESSIONTimeout = CreateTimeSpan(0, 0, 30, 0)>
	<cfset THIS.ClientManagement = false>
	<cfset THIS.SetClientCookies = true>
	<cfset THIS.datasource = "ePort">
	<cfset THIS.customtagpaths = GetDirectoryFromPath(GetCurrentTemplatePath()) & "customtags\">
	<cfset THIS.mappings["/CFCs"] = GetDirectoryFromPath(GetCurrentTemplatePath()) & "CFCs\">

	<cffunction
			name="OnApplicationStart"
			access="public"
			returntype="boolean"
			output="false"
			hint="Fires when the APPLICATION is first created.">

			<!---TODO: make work! Workaround for this.mappings and application.mappings still not allowing invocation of cfcs was to make a mapping in cfadmin--->
			<!---<cfset Application.mappings["/CFCs"] = GetDirectoryFromPath(GetCurrentTemplatePath()) & "CFCs/">--->
			<cfset Application.basePath = GetDirectoryFromPath(GetCurrentTemplatePath())>
			<cfimport prefix="mytags" taglib="customtags">

			<cfset var qGetAppID = "">
			<cfquery name="qGetAppID" datasource="ePORT">
				SELECT 	App_ID
				FROM 	Applications
				WHERE 	App_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#application.applicationname#">
			</cfquery>
		<!--- This is a global setting that is the same regardless of the environment --->
		<cfset APPLICATION.App_ID = qGetAppID.App_ID/>
		<cfif cgi.server_name IS "127.0.0.1" OR cgi.server_name IS "localhost">
			<cfset APPLICATION.environment = "Development">
		<!--- workaround for local login --->
			<cfset APPLICATION.localUserName = "FDA\David.Jacobson">
			<cfsetting
					requesttimeout="20"
					showdebugoutput="true"
					enablecfoutputonly="false"/>
			<cfset application.SystemRoot = 'http://' & cgi.server_name & '/portal/www' />

		<cfelseif FindNoCase("dev.fda.gov", cgi.server_name)>
			<cfset APPLICATION.environment = "SharedDev">
			<cfsetting
					requesttimeout="45"
					showdebugoutput="true"
					enablecfoutputonly="false"/>

		<cfelseif FindNoCase("test.fda.gov", cgi.server_name)>
			<cfset APPLICATION.environment = "Test">

			<cfelseif FindNoCase("preprod.fda.gov", cgi.server_name)>
			<cfset APPLICATION.environment = "Preprod">
		<cfelse>
			<cfset APPLICATION.environment = "Prod">
		</cfif>

		<cfif APPLICATION.environment IS NOT "Development">
		<!--- IN order to communicate with the FDA LDAP server, this component must be called.  However, it will only work on FDA servers, so we don't call it on localhost --->
			<CFSET THIS.components.globalVars = createObject("component", "globalVariables")>
			<CFSET THIS.components.globalVars.setGlobalVars(GetDirectoryFromPath(GetCurrentTemplatePath()))>
			<cfset application.SystemRoot = 'http://' & cgi.server_name & '/scripts/ePort'/>
		</cfif>

		<cfif NOT structKeyExists(APPLICATION,'baseURL') OR (structKeyExists(APPLICATION,'baseURL') AND NOT LEN(TRIM(APPLICATION.baseURL))) OR structKeyExists(URL,'resetBaseURL')>
			<cfif cgi.server_name IS "127.0.0.1" OR cgi.server_name IS "localhost">
				<cfset APPLICATION.BaseURL = "http://"& cgi.server_name & "/portal/www/eRotation">
			<cfelse>
				<cfset APPLICATION.BaseURL = "https://" & cgi.server_name & "/scripts/ePort/eRotation">
			</cfif>
		</cfif>

		<cfset APPLICATION.ListAllowedFileTypes = "pdf,xls,xlsx,doc,docx,txt,eml,one,ppt,pptx,vsd,zip,rar,csv,gif,jpg,png">
		<cfset APPLICATION.ListAllowedMimeTypes = "APPLICATION/pdf,APPLICATION/vnd.ms-excel,APPLICATION/vnd.openxmlformats-officedocument.spreadsheetml.sheet,APPLICATION/vnd.openxmlformats-officedocument.wordprocessingml.document,APPLICATION/vnd.openxmlformats-officedocument.wordprocessingml.document,text/plain,message/rfc822,APPLICATION/onenote,
APPLICATION/vnd.ms-powerpoint,APPLICATION/vnd.openxmlformats-officedocument.presentationml.presentation,APPLICATION/vnd.visio,APPLICATION/zip,APPLICATION/x-rar-compressed,text/csv,image/gif,image/jpeg,image/png">

		<cfreturn true/>
	</cffunction>

	<cffunction
			name="onRequestStart"
			access="public"
			returntype="void"
			output="false"
			hint="Fires each time a request is made">

		<cfparam name="REQUEST.maxRowsToQuery" default=1000>
		<cfparam name="REQUEST.maxRowsToDisplay" default=100>
		
		<cfset request.supportEmail = 'OO-OHCM-eRotation@fda.hhs.gov'>

		<!--- RESET APP? --->
		<cfif structKeyExists(url,'reinit')>
			<cfset OnApplicationStart()>
			<cfset OnSessionStart()>
		</cfif>

	</cffunction>

	<cfif cgi.REMOTE_ADDR IS NOT "127.0.0.1">

<!--- START - THIS LINE SHOULD BE COMMENTED ON LOCALHOST BUT NOWHERE ELSE! DO NOT COMMIT TO GIT --->
<!---<cffunction name="onError">
<!--- END - THIS LINE SHOULD BE COMMENTED ON LOCALHOST BUT NOWHERE ELSE! DO NOT COMMIT TO GIT --->
	<cfargument name="Exception" required=true/>
	<cfargument type="String" name="EventName" required=true/>

	<cfinclude template="error.cfm">

<!--- START - THIS LINE SHOULD BE END-COMMENTED ON LOCALHOST BUT NOWHERE ELSE! DO NOT COMMIT TO GIT --->
</cffunction>--->
<!--- END - THIS LINE SHOULD BE END-COMMENTED ON LOCALHOST BUT NOWHERE ELSE! DO NOT COMMIT TO GIT --->
	</cfif>
	<cffunction name="onMissingTemplate">
		<cfargument name="targetPage" type="string" required="true"/>
<!--- Use a try block to catch errors. --->
		<cftry>
<!--- Display an error message. --->
			<cf_error_404>
<!---TODO: remove debug--->
			<cfreturn true/>

<!--- If an error occurs, return false and the default error
handler will run. --->
			<cfcatch>
				<cfdump var="#cfcatch#" label="cfcatch" expand="true" abort="false"/>
				<cfreturn false/>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction
			name="OnSessionStart"
			access="public"
			returntype="void"
			output="false"
			hint="I fire when the Session is started">

		<cfcookie name="expType" value="exp90"/>
	</cffunction>

	<cffunction
			name="OnSessionEnd"
			access="public"
			returntype="void"
			output="false"
			hint="Fires when the SESSION is terminated.">
<!--- Define arguments. --->
		<cfargument
				name="SessionScope"
				type="struct"
				required="true"/>

		<cfargument
				name="ApplicationScope"
				type="struct"
				required="false"
				default="#StructNew()#"/>
<!--- Return out. --->
		<cfreturn/>
	</cffunction>
</cfcomponent>