<cfcomponent displayname="FDA Employee Profile Processes" hint="ColdFusion Component for processing Employee Profiles">

    <cffunction access="public" name="GetAuthentication" hint="Gets Login Authentication" returntype="query">

        <cfargument name="user" type="string"required="true">

        <cfset var qGetAuthentication = "">
        <cfquery name="qGetAuthentication">
        SELECT 	Employees.First_Name, Employees.Last_Name, Employees.Display_Name, Employees.isActive, Employees.NED_ID,
                Employees.last_logged_in, Employees.Job_Title, Employees.email_address, Employees.Office, Employees.office_id,
                Employees.fda_username, Employees.Employee_UUID, Employees.Reports_To, Employees.reports_to_name,
                Employees.isCommissionedCorps, Employees.isExecutive, Supervisors.NED_ID AS Supervisor_NED_ID,
                Supervisors.First_Name + ' ' + Supervisors.Last_Name AS Supervisor_Full_name, FC.Center_ID,
                FC.Center_Acronym, FC.Center_Name, Applications.App_Name, Employee_Roles.Role_Name
        FROM 	Employees INNER JOIN
                Employees_To_Roles ON Employees.CapHR_ID = Employees_To_Roles.CapHR_ID INNER JOIN
                Applications ON Employees_To_Roles.App_id = Applications.App_id INNER JOIN
                Employee_Roles ON Employees_To_Roles.Role_ID = Employee_Roles.Role_ID LEFT OUTER JOIN
                Employees AS Supervisors ON Employees.Reports_To = Supervisors.NED_ID LEFT OUTER JOIN
                FDA_Centers AS FC ON Employees.center_ID = FC.Center_ID
        WHERE 	Applications.App_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#application.applicationname#"> AND
                Employees.fda_username = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.user#"> AND
                Employees.IsActive = 1
        </cfquery>

        <cfreturn qGetAuthentication />

    </cffunction>

    <cffunction access="public" name="InsertLastLogin" hint="Add Login to table">

        <cfargument name="userID" type="string" required="true">

        <cfset var qAddUserLogin = "">
        <cfquery name="qAddUserLogin">
        INSERT INTO  Employee_Logins
            (login_date, remote_address, capHR_ID, app_id)
        VALUES
            (<cfqueryparam cfsqltype="cf_sql_timestamp" value="#NOW()#">,
             <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.remote_addr#">,
             <cfqueryparam cfsqltype="cf_sql_varchar" value="#ARGUMENTS.userID#">,
             <cfqueryparam cfsqltype="cf_sql_integer" value="#APPLICATION.App_ID#">)
        </cfquery>

    </cffunction>

</cfcomponent>