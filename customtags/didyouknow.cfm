<cfparam name="attributes.message" default="You can edit &amp; save this information">

<div class="panel panel-success">
	<div class="panel-heading" style="color:black">
		<cfoutput>
			<div class="row">
				<div class="col-sm-12 text-center">
					<i class="fa fa-hand-o-up"></i> <i style="font-size: small">#attributes.message#</i>
				</div>
			</div>
		</cfoutput>
	</div>
</div>
