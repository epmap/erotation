<cfparam name="Request.RichTextClassesList" default="">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<!---Devnote: Ignore error on above line. It will be flagged as an error because most IDE's and syntax checkers won't detect that this is a page fragment and complain about an opening tag whose end tag is in another file.--->

<cfoutput>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=11">
        <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><cfif isDefined('attributes.title')>#attributes.title#<cfelse>FDA eRotation</cfif></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="#APPLICATION.BaseURL#/assets/img/favicon.ico" />
    <meta name="description" content="FDA HHS eRotation System - Developed by Gap Solutions, Inc.">

    <!-- Global site tag (gtag.js) - Google Analytics following will also error and may be flagged as deprecated by checkers -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155887137-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-155887137-1');
    </script>

    <!-- End Google Tag Manager -->

        <!--- LESS --->
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/less/checkmark.css">

        <!--- jquery  - IMPORTANT - MUST COME FIRST! --->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

        <!--- VENDORS.JS --->
        <script src="#APPLICATION.BaseURL#/assets/js/vendors.js"></script>
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/vendors.css">

        <!--- jQueryUI --->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!--- Moment.js --->
        <script src="#APPLICATION.BaseURL#/assets/js/moment.min.js"></script>

        <!--- Bootstrap CSS --->
        <!--- Remember, DO NOT INCLUDE BOOTSTRAP JS FILE AS IT'S COMPILED IN VENDORS.JS--->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/style-blue.css" title="default">
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/width-full.css" title="default">
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/bootstrap-select.css" title="default">

        <!--- Font Awesome 4.7.0 --->
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/font-awesome.min.css">

        <!--- RichText editor --->
        <link rel="stylesheet" href="#APPLICATION.BaseURL#/assets/css/richtext.min.css">
        <script src="#APPLICATION.BaseURL#/assets/js/jquery.richtext.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="#APPLICATION.BaseURL#/assets/js/html5shiv.min.js"></script>
        <script src="#APPLICATION.BaseURL#/assets/js/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="#APPLICATION.BaseURL#/assets/css/cfdump.css">
        <script src="#APPLICATION.BaseURL#/assets/js/app.js"></script>

<style>
.header-full {
    <cfif isUserInAnyRole('Developer,Admin') AND Application.Environment IS NOT "Production">
            /* If we are not on Prod, warn us */
        background-image: url("#APPLICATION.BaseURL#/assets/img/back.jpg");
        box-shadow: inset 0 0 0 2000px rgba(0, 0, 0, .5);
    <cfelse>
    background-image: url("#APPLICATION.BaseURL#/assets/img/back_lead6.jpg");
        box-shadow: inset 0 0 0 2000px rgba(0, 89, 126, 0.5);
    </cfif>

    <cfif structKeyExists(session,'proxyuser') AND session.proxyuser EQ 1>
            /* If we are proxied in, warn us */
        background-image: url("#APPLICATION.BaseURL#/assets/img/back.jpg");
        box-shadow: inset 0 0 0 2000px rgba(255, 136, 42, .7);
    </cfif>
        background-repeat: no-repeat;
        background-size: cover;
<!--- OVERRIDE for text-transform found in the stylesheets that forced heading to lowercase --->
        .navbar-default,
        .navbar-brand {
        text-transform: none;
 }
</style>

</head>
</cfoutput>

<body>
<cfif isDefined('request.SiteMessage') AND trim(request.SiteMessage) NEQ "" AND NOT isUserInAnyRole("Developer,Admin")>
	<cfoutput>#request.SiteMessage#</cfoutput>
	<cfelseif isDefined('request.SiteMessage') AND trim(request.SiteMessage) NEQ "" AND isUserInAnyRole("Developer,Admin")>
    <div class="em-danger-inverse text-center" style="color:white">
        <i class="fa fa-warning"></i> SITE HEADER IS ACTIVE TO USERS
    </div>
</cfif>

<cfif structKeyExists(session, 'authenticated') AND session.authenticated EQ 1>
    <div style="height: 2px; background-color: #0eb7ff" title="You are logged in"></div>
<cfelse>
    <div style="height: 20px; background-color: red; color:#000000;" title="Not logged in"></div>
</cfif>

<div class="sb-site-container">

<div class="boxed">

<header id="header-full-top" class="hidden-xs header-full">

    <div class="container-fluid" style="border-bottom: 1px solid #040404">
        <div class="row">
            <div class="col-lg-6 header-full-title">
                <cfoutput>
                    <h4 class="animated fadeInRight" style="font-weight:400; font-size:24px; color: white">FDA eRotation<br>
                        Electronic Rotation Opportunity Management Program</h4>
                </cfoutput>
            </div>
            <div class="col-lg-6 text-right">
                <cfoutput>
                    <cfif structKeyExists(session, 'user_id') AND (listLast(cgi.SCRIPT_NAME, "/") IS NOT "logout.cfm" AND listLast(cgi.SCRIPT_NAME, "/") IS NOT "index.cfm")>

                        <cfset variables.profileBgColor = "white">

                        <cfif isUserInAnyRole("Developer,Admin")>
                            <cfset variables.profileBgColor = "yellow">
                        </cfif>
                        <cfif structKeyExists(session, 'proxyUser') AND session.ProxyUser EQ 1>
                            <cfset variables.profileBgColor = "orange">
                        </cfif>
                        <br>
                        <span style="color:#variables.profileBgColor#; text-transform: capitalize; font-size: 20px">
                        #session.username#<br>#session.employee_roles#
                        </span>
                    </cfif>
                </cfoutput>
            </div>
        </div>
    </div>

</header>

<cfif listLast(cgi.SCRIPT_NAME, "/") NEQ "login.cfm" and listLast(cgi.SCRIPT_NAME, "/") NEQ "logout.cfm">
<nav class="navbar navbar-default navbar-header-full navbar-inverse yamm navbar-static-top" role="navigation" id="header">
    <div class="container-fluid"  style="width:100%">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header primary-color">
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">FDA <span>eRotation</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="row" id="bs-example-navbar-collapse-1">
            <cfoutput>
                <ul class="nav navbar-nav" style="width: 100%;">
                <li>
			    <cfif isDefined('session.authenticated') AND session.authenticated EQ 1>
                    <a href="#application.BaseURL#/auth/summary.cfm"><i class="fa fa-home" style="padding-right: 5px"></i>Summary</a>
                    <cfif isUserInAnyRole("Developer,Admin")>
                            <li class="dropdown text-right">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-wrench"></i>&nbsp; Tools</a>
                                <ul class="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                                    <li role="presentation" class="dropdown-header" style="font-size: 16px; background: darkgray">User Tools</li>
                                    <li><a href="#application.baseURL#/tools/manage_users.cfm"><i class="fa fa-users" style="margin-right: 5px"></i>Manage Users</a></li>
                                    <li role="presentation" class="dropdown-header" style="font-size: 16px; background: darkgray">Reports</li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-bar-chart" style="margin-right: 5px"></i>Coming Soon .....</a>
                                </ul>
                            </li>
                    </cfif>
                    <li class="col-sm-1"></li>
                    <li class="col-sm-3"></li>
                <li class="col-sm-1"></li>
                </cfif>
                <li class="pull-right text-right">
                <cfif isDefined('session.authenticated') AND session.authenticated EQ 1>
                    <a href="#application.baseURL#/auth/logout.cfm" title="Logout"><i class="fa fa-sign-out"></i> Logout</a>
                <cfelse>
                    <a href="#application.BaseURL#/login.cfm" title="Login"> <i class="fa fa-sign-in"></i> Login</a>
                </cfif>
            </li>
        </ul>
</cfoutput>
</div><!-- navbar-collapse -->
</nav>
</cfif>
<cfset request.headerComplete = true>