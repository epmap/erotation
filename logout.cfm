<cfheader name= 'expires' value='#Now()#'>
<cfheader name= 'pragma' value='no-cache'>
<cfheader name= 'cache-control' value='no-cache, no-store, must-revalidate'>
<head>
	<meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0">
</head>
<cflock scope="SESSION" timeout="10" type="exclusive">
	<cfset structclear(SESSION)>
</cflock>
<cfloop
		index="local.cookieName"
		list="cfid,cftoken,cfmagic">
	<cfcookie
			name="#local.cookieName#"
			value=""
			expires="now"/>
</cfloop>
<cfapplication name="eRotation"
		SESSIONmanagement="Yes"
		SESSIONtimeout="#CreateTimeSpan(0, 0, 0, 0)#">
<meta http-equiv="REFRESH" content="1; URL=/Portal/www">
<cfset this.SESSIONTimeout = createTimeSpan(0, 0, 0, 1)>
<cfabort>
