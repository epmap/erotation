<cfcomponent displayname="Dashboard Queries" hint="ColdFusion Component for processing and returning Dashboard Data and functions">

    <cffunction name="qGetDashboardEmployees" access="public" returntype="query" hint="Get Employees for Employee tab for logged in user by Employee Role">
	    <cfargument name="User_ID"      required="yes">
	    <cfargument name="first_name"   required="no" default="">
	    <cfargument name="last_name"    required="no" default="">
	    <cfargument name="supervisor"   required="no" default="0">
	    <cfargument name="center"       required="no" default="" type="numeric">
	    <cfargument name="myEmployee"   required="no" default="0">
		<cfargument name="startRow"		required="no" default="0">
		<cfargument name="roles"		    required="no" default="All">

	    <cfset var qGetDashboardEmployees = "">
		<cfquery name="qGetDashboardEmployees" maxrows="#request.maxRowsToQuery#">
			SELECT 	Employees.NED_ID, Employees.CapHR_ID, Employees.Last_Name, Employees.First_Name, Employees.Job_Title,
					Employees.Reports_To, Employees.email_address, Employees.center_ID, Employees.fda_username,
					Employees.Employee_UUID, FDA_Centers.Center_Name, FDA_Centers.Center_Acronym, FDA_Offices.office_name,
					Supervisors.First_Name + '  ' + Supervisors.Last_Name AS Supervisor, Supervisors.email_address AS Supervisor_email,
					Supervisors.Phone AS Supervisor_phone, Supervisors.CapHR_ID AS Supervisors_CapHR_ID, Supervisors.Job_Title AS Supervisor_Job_Title,
					Supervisor_Center.Center_Name AS Supervisor_Center_Name, Supervisor_FDA_Offices.office_name AS Supervisors_Office_Name,
					Supervisor_Center.Center_Acronym AS Supervisor_Center_Acronym, Employees.Phone,
					CASE WHEN Employee_Roles.Role_Name IS NULL THEN 'No Role' ELSE Employee_Roles.Role_Name END AS Role_Name,
					Employees_To_Roles.App_id, Employees_To_Roles.isActive
			FROM  	FDA_Offices AS Supervisor_FDA_Offices RIGHT OUTER JOIN
					Employees AS Supervisors ON Supervisor_FDA_Offices.office_id = Supervisors.office_id RIGHT OUTER JOIN
					FDA_Offices RIGHT OUTER JOIN
					Employees WITH (NOLOCK) ON FDA_Offices.office_id = Employees.office_id LEFT OUTER JOIN
					Employee_Roles INNER JOIN
					Employees_To_Roles ON Employee_Roles.Role_ID = Employees_To_Roles.Role_ID ON Employees.CapHR_ID = Employees_To_Roles.CapHR_ID ON
					Supervisors.CapHR_ID = Employees.Reports_To LEFT OUTER JOIN
					FDA_Centers ON FDA_Centers.Center_ID = Employees.center_ID LEFT OUTER JOIN
					FDA_Centers AS Supervisor_Center ON Supervisor_Center.Center_ID = Supervisors.center_ID
			WHERE 	Employees.CapHR_ID > 0 AND (Employees_To_Roles.isActive = 1 OR Employees_To_Roles.isActive IS NULL) AND
					(Employees_To_Roles.App_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#application.app_id#"> OR Employees_To_Roles.App_id IS NULL)
					<cfif (structKeyExists(arguments,'center') AND val(arguments.center) GT 0)>
						AND Employees.center_ID = (<cfqueryparam cfsqltype="cf_sql_varchar" value="#val(arguments.center)#" />)
					</cfif>
					<cfif LEN(TRIM(arguments.first_name))>
						AND Employees.First_Name LIKE <cfqueryparam cfsqltype="cf_sql_varchar"  value="#trim(arguments.first_name)#%">
					</cfif>
					<cfif LEN(TRIM(arguments.last_name))>
						AND Employees.Last_Name LIKE <cfqueryparam cfsqltype="cf_sql_varchar"  value="#trim(arguments.last_name)#%">
					</cfif>
			ORDER BY    Employees.Last_Name, Employees.First_Name
	    </cfquery>

	<cfreturn qGetDashboardEmployees />

    </cffunction>

    <cffunction name="qSearchAllEmployees" access="public" returntype="query" hint="Search all employees">
	    <cfargument name="first_name"   required="no" default="">
	    <cfargument name="last_name"    required="no" default="">
	    <cfargument name="supervisor"   required="no" default="0">
	    <cfargument name="center"       required="no" default="0">
		<cfargument name="startRow"		required="no" default="1">
		<cfargument name="roles"		    required="no" default="All">

	    <cfset var qSearchAllEmployees = "">
		<cfquery name="qSearchAllEmployees" maxrows="#request.maxRowsToQuery#">
		SELECT      CASE WHEN Employees.CapHR_ID IS NULL
				THEN Employees.NED_ID
				ELSE Employees.CapHR_ID
			    END AS CAPHR_ID,
			    Employees.Last_Name, Employees.First_Name, Employees.Job_Title, Employees.Reports_To, Employees.email_address, Employees.Employee_Roles,
			    Employees.Phone, Employees.center_ID, Employees.isActive, Employees.last_logged_in, Employees.fda_username, Employees.Employee_UUID,
				FDA_Centers.Center_Name, FDA_Centers.Center_Acronym,
				Supervisors.First_Name + '  ' + Supervisors.Last_Name AS Supervisor, Supervisors.email_address AS Supervisor_email, Supervisors.Phone AS Supervisor_phone,
				Supervisors.Employee_UUID as Supervisors_Employee_UUID, Supervisor_Center.Center_Acronym AS Supervisor_Center_Acronym,
							Supervisor_Center.Center_Name AS Supervisor_Center_Name, Supervisors.CapHR_ID AS Supervisors_CapHR_ID,
			    (SELECT COUNT(PMAP_ID) from PMAPS WHERE PMAPS.PMAP_EMPLOYEE_NED_ID = Employees.ned_id) AS PMAP_Count
		FROM        EMPLOYEES
				LIMIT <cfqueryparam value="#startRow#" cfsqltype="cf_sql_integer" />,<cfqueryparam value="#request.maxRowsToQuery#" cfsqltype="cf_sql_integer" />
				LEFT OUTER JOIN FDA_Centers 						ON FDA_Centers.Center_ID = Employees.center_ID
				LEFT OUTER JOIN FDA_Centers AS Supervisor_Center    ON FDA_Centers.Center_ID = Supervisor_Center.center_ID
				LEFT OUTER JOIN Employees AS Supervisors   			ON Supervisors.NED_ID = Employees.Reports_To
		WHERE NED_ID > 0
		<cfif LEN(trim(arguments.first_name))>
		    AND Employees.First_Name LIKE <cfqueryparam cfsqltype="cf_sql_varchar"  value="#trim(arguments.first_name)#%">
		</cfif>
		<cfif LEN(trim(arguments.last_name))>
		    AND Employees.Last_Name LIKE <cfqueryparam cfsqltype="cf_sql_varchar"  value="#trim(arguments.last_name)#%">
		</cfif>
		<cfif arguments.supervisor GT 0>
		    AND Employees.Reports_To IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#TRIM(arguments.supervisor)#" />)
		</cfif>
		<cfif arguments.roles IS NOT "All">
		    AND Employees.Employee_Roles =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.roles#" />
		</cfif>
		<cfif arguments.center GT 0>
		    AND Employees.Center_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" list="true" value="#TRIM(arguments.center)#" />)
		</cfif>
		<cfif isUserInRole("Rating_Official,Reviewing_Official,Coordinator")>
		    <!--- RTOs, RVO,s and PC's can only see employees within their own center --->
		    AND Employees.center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(session.Employee_Center_ID)#">
		</cfif>
		ORDER BY    Employees.Last_Name, Employees.First_Name

	    </cfquery>

	<cfreturn qSearchAllEmployees />

    </cffunction>

</cfcomponent>