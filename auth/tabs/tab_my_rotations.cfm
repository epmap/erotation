
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-6">
        <cfif NOT isUserInAnyRole("Employee")>
            <cfoutput>
                    <a href="#application.baseURL#/Auth/RotationOpportunityEntryForm.cfm">
                    <button type="submit" name="btnAddNewRotation" id="btnAddNewRotation" value="Submit" class="btn btn-default navbar-btn relocright">
                        Add Rotation Opportunity
                    </button>
                </a>
            </cfoutput>
        </cfif>
    </div>
    <div class="col-md-6">
        <cfoutput>
            <form action="Summary.cfm?#cgi.query_string#" method="post">
            <label for="fltr_centerOffice">Filter by Center</label>
            <select name="fltr_centerOffice" id="fltr_centerOffice" class="form-control" data-live-search="true" data-size="10" data-dropup-auto="false" title="Select center, click Refresh" onchange="$(this.form).trigger('submit')">
            <option value="0">All Centers</option>
            <cfloop query="rtn_getActiveCenters">
            <option value="#rtn_getActiveCenters.Center_ID#" <cfif FORM.fltr_centerOffice EQ rtn_getActiveCenters.Center_ID>selected</cfif>>#rtn_getActiveCenters.Center_Acronym#</option>
            </cfloop>
            </select>
            </form>
        </cfoutput>
    </div>
</div>

<cfif rtn_MyRotations.recordcount GTE 1>

        <table class="display table table-striped table-responsive small" id="SupRotations">
            <thead>
            <tr class="success" role="row">
                <th>&nbsp;</th>
                <th align="center">OPP&nbsp;#</th>
                <th>STATUS</th>
                <th>HOST SUPERVISOR</th>
                <th>CENTER/OFFICE</th>
                <th>OPPORTUNITY TITLE</th>
                <th width="40%">DESCRIPTION</th>
                <th>CLEARANCE</th>
                <th>PARTICIPANT LOCATION</th>
            </tr>
            </thead>
            <tbody>
            <cfoutput Query="rtn_MyRotations">

                <cfquery name="qry_GetRotationCenter" >
                    SELECT  Center_Name, Center_Acronym
                    FROM    FDA_Centers
                    WHERE   Center_ID = <cfqueryparam value="#rtn_MyRotations.centerOffice#" cfsqltype="cf_sql_integer" null="#NOT(trim(len(rtn_MyRotations.centerOffice)))#">
                </cfquery>

                <tr style="min-height: 80px; text-transform: capitalize; color: black">
                    <td>
                        <button class="btn btn-info text-center" onclick="window.location='RotationOpportunityDetail.cfm?rotOpp_ID=#rtn_MyRotations.rotOpp_ID#'"><i class="fa fa-search"></i>
                        </button>
                    </td>
                    <td align="center">#rtn_MyRotations.rotOpp_ID#</td>
                    <td <cfif rtn_MyRotations.rotationOppStatus IS NOT 'Open'> style="color: red; font-weight: bold" </cfif>>#ucase(rtn_MyRotations.rotationOppStatus)#</td>
                    <td>#rtn_MyRotations.hostSupervisorName#</td>
                    <td>#qry_GetRotationCenter.center_Acronym#</td>
                    <td>#rtn_MyRotations.rotationTitle#</td>
                    <td>#rtn_MyRotations.justification#</td>
                    <td>#rtn_MyRotations.clearencesRequired#</td>
                    <td>#rtn_MyRotations.participantLocation#</td>
                </tr>
            </cfoutput>
        </table>
    <cfelse>
        <h5 style="font-weight: 400">You have NOT applied for any rotation opportunities.</h5>
    </cfif>
