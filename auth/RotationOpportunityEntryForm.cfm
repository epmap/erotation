<cf_header title="Rotation Opportunity Entry" noNav=true>

<style>

    .grayLabel {
        background-color: #d6dde3;
        color: #030033;
        font-size: 1.6rem;
        vertical-align: text-bottom;
        horiz-align: center;
        width: 15%;
    }

    table, th, td {

        border: 2px solid black;
        padding: 10px;
    }

</style>
<cfset variables.FORM = []/>
<cftry>
		<cfinvoke component="CFCs/FDAData" method="getActiveCenters" returnvariable="rtn_getActiveCenters">
        <cfinvoke component="CFCs/FDAData" method="getAllOffices" returnvariable="rtn_getAllOffices">

    <cfcatch>
		<!---<cfif SESSION.environment NEQ "Development" and SESSION.environment NEQ "SharedDev">
			<cferror type="exception" exception="any" template="error.cfm">
		<cfelse>--->
            <h2>Something Happened!</h2>
			<cfdump var="#cfcatch#" label="cfcatch" expand="true" abort="false"/>
		<!---</cfif>--->
	</cfcatch>
</cftry>
<cfinvoke component="CFCs/employee" method="qGetEmployeeData" UUID ="#numberFormat(session.user_ID, "0000000")#" returnvariable="rtn_qGetEmployeeData">

<cfif NOT rtn_qGetEmployeeData.RecordCount>
    <div class="container">
	<mytags:checkmark type="Warning" Message="Account not found" submessage="Please contact the PMAP support team if you need more assistance." />
    </div>
</cfif>
<div class="container-fluid" style="margin: 0px 25px 0px 25px">
    <div class="row">
        <h2 style="font-weight: 400">FDA&nbsp;Rotation&nbsp;Opportunity</h2>
    </div>
    <a href="Summary.cfm">
        <button type="button" class="btn btn-primary">
            Return to Summary
        </button>
    </a><BR><BR>
    <cfoutput>
    <form name="frm_RotationEntry" accept-charset="UTF-8" action="RotationOpportunity_Save.cfm" autocomplete="off" method="POST">
    <fieldset>
<!---TODO: add session userID for hostSupervisorCapHR_ID and populate--->
    <input type="hidden" name="hostSupervisorCapHRID" class="form-control" id="hostSupervisorCapHRID"
            value="#session.user_id#"/>
        <input type="hidden" name="hostSupervisorSignDate"
               id="hostSupervisorSignDate" value="#dateformat(now(), 'yyyy-mm-dd')#"/>
        <input type="hidden" name="dateEntered" id="dateEntered" value="#dateformat(now(), 'yyyy-mm-dd')#"/>
<table>
<tr>
<td colspan="4" class="grayLabel">
        <hr style="height:4px; color:##ff0000; background-color:##ff0000;"/>
    <em>FDA rotation opportunities, particularly those which emphasize cross-Center/Office assignments, provide FDA leaders with quality developmental experiences and tools to achieve both mission and career success. FDA leaders, both participants and hosts, can share their expertise and work with others to respond to FDA's mission neeeds and strategic priorities.</em>
</td>
</tr>
<tr>
<td colspan="4" class="grayLabel"><strong>Instructions:</strong> This form is designed as a resource to assist host
    supervisors in establishing experiential opportunities that support mission operations. All rotation assignments will be posted internally on the FDA eRotations system. FDA rotations established for the President's Management Council (PMC) Fellowship will be shared externally with HHS and Federal partners.<BR>
        <hr style="height:4px; color:##ff0000; background-color:##ff0000;"/>
</td>
</tr>

<tr align="left">
    <td class="grayLabel">
        <label for="centerOffice">FDA Center/Office:</label>
    </td>
<td>
    <select name="centerOffice" id="centerOffice" class="form-control " required="true" data-live-search="true" data-size="25" data-dropup-auto="false"
    <cfif val(rtn_qGetEmployeeData.Center_ID) EQ 0> data-style="btn-danger" </cfif>>
    <option value="">Select Center/Office ...</option>
    <cfloop query="rtn_getActiveCenters">
    <option value="#rtn_getActiveCenters.Center_ID#" <cfif rtn_qGetEmployeeData.Center_ID EQ rtn_getActiveCenters.Center_ID>selected</cfif>>#rtn_getActiveCenters.Center_Acronym# - #rtn_getActiveCenters.Center_Name#</option>
    </cfloop>
    </select>
</td>
        <td class="grayLabel" colspan="1"><label for="subComponent">Sub-Office/Division - Sub-Component</label></td>
    <td style="border-top: 0px">
    <select name="subComponent" id="subComponent" class="form-control" title="Select Office ....." data-live-search="true" data-size="25" data-dropup-auto="false">
    <cfoutput>
        <option value="" SELECTED>Select Sub-Office/Division...</option>
        <cfloop query="rtn_GetAllOffices">
            <option data-option="#rtn_getAllOffices.Center_ID#" value="#rtn_GetAllOffices.Office_ID#" <cfif rtn_qGetEmployeeData.office_id EQ rtn_GetAllOffices.office_id>selected</cfif>>&nbsp;&nbsp;&nbsp;#rtn_GetAllOffices.Office_Name#</option>
        </cfloop>
    </cfoutput>
    </select>
    </td>
    </tr>
    <tr>
<!---background-color:  ##d6dde3;"--->
        <td colspan="4" class="grayLabel"><label for="justification">Specify how this rotation aligns with FDA's strategic goals and supports workforce planning for the Agency:</label></td>
    </tr>
        <tr>
            <td colspan="4">
                <textarea id="justification" name="justification" class="form-control" required placeholder="How this rotation aligns with FDA's strategic goals and supports workforce planning for the Agency goes here."></textarea>
            </td>
        </tr>

        <tr>
            <td class="grayLabel">
                <label for="rotationTitle">Rotation Title</label>
            </td>
            <td>
                <input id="rotationTitle" name="rotationTitle" type="text" value=""
                       placeholder="GS-13 Hospital Administrator" class="form-control" required="true" />
            </td>
            <td class="grayLabel">
                <label for="clearencesRequired">Required Clearances</label>
            </td>
            <td>
                <input name="clearencesRequired" id="clearencesRequired" type="text" value=""
                       placeholder="Like Trusted?" class="form-control"/>
            </td>
        </tr>
        <tr>
            <td class="grayLabel">
                <label for="numPositions">Number of Positions:</label>
            </td>
            <td align="center">
                <input name="numPositions" id="numPositions" type="number" value="1"
                       placeholder="placeholder?" class="form-control"/>
            </td>
            <td class="grayLabel" rowspan="2">
                <label for="officeAddress">Office Address:</label></td>
            <td rowspan="2">
			<textarea name="officeAddress" id="officeAddress" class="form-control" placeholder="123 Sesame St.
Carlislye, PA 20171" class="form-control"></textarea>
            </td>
        </tr>
        <tr>
            <td class="grayLabel">
                <label for="execSponsor">Executive Sponsor:</label>
            </td>
            <td>
                <input name="execSponsor" id="execSponsor" type="text" value=""
                       placeholder="Name of Exec" class="form-control"/>
            </td>
        </tr>
        <tr>
            <td class="grayLabel">
                <label for="hostSupervisorName">Host Supervisor Name: </label>
            </td>
            <td><input name="hostSupervisorName" id="hostSupervisorName" type="text" value=""
                       placeholder="Your name" class="form-control" required="true"/></td>
            <td class="grayLabel">
                <label for="participantLocation">Participant Location: </label>
            </td>
            <td>
                <select name="participantLocation" id="participantLocation" class="form-control">
                    <option selected="selected" value="0">Select...</option>
                    <option value="Virtual">Virtual</option>
                    <option value="In-Person">In-Person</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="grayLabel">
                <label for="hostSupervisorEmail">Host&nbsp;Supervisor Email:</label>
            </td>
            <td>
                <input name="hostSupervisorEmail" id="hostSupervisorEmail" type="text" value="" placeholder="name@FDA.gov" class="form-control" required="true" />
            </td>
            <td class="grayLabel">
                <label for="hostSupervisorTitle">Host Supervisor Title:</label>
            </td>
            <td>
                <input name="hostSupervisorTitle" id="hostSupervisorTitle" type="text" value="" class="form-control"/>
            </td>
        </tr>
        <tr>
            <td class="grayLabel">
                <label for="hostSupervisorPhone">Host&nbsp;Supervisor Phone:</label>
            </td>
            <td>
                <input name="hostSupervisorPhone" id="hostSupervisorPhone" type="text" value=""
                       placeholder="703.555.1212" class="form-control"/>
            </td>
            <td class="grayLabel">
                <label for="agencyPOC">Agency POC:</label>

            </td>
            <td>
                <!---One for show, and one to go--->
                <input name="agencyPOC" id="agencyPOC" type="text" disabled="disabled" value="OO-OHCM-eRotation@fda.hhs.gov" class="form-control"/>
                <input name="agencyPOC" id="agencyPOC" type="hidden" value="OO-OHCM-eRotation@fda.hhs.gov" class="form-control"/>
            </td>
        </tr>
    </table>
        <table>
            <tr>
                <td class="grayLabel" colspan="1">
                    <label for="workplaceFlexibilities">Available Workplace Flexibilities:</label>
                </td>
                <td colspan="3">
                    <input name="workplaceFlexibilities" id="workplaceFlexibilities" type="text" value=""
                           placeholder="Capital Region, Remote, New York, Medical Ships" class="form-control"/>
                </td>
            </tr>
            <tr>
                <td class="grayLabel" colspan="1">
                    <label for="newRotationHost">I am a new FA Rotation Host<BR>
                        <em style="font-size: smaller">(new to hosting a rotational assignment)</em>
                    </label>
                </td>
                <td width="10%"><input type="radio" id="yes" name="newRotationHost"  value="1">&nbsp;&nbsp;
                    <label for="yes">YES</label>&nbsp;&nbsp;
                </td>
                <td width="10%">
                    <input type="radio" id="no" name="newRotationHost" checked="true" value="0">&nbsp;&nbsp;
                    <label for="no">NO</label>
                </td>
            </tr>
            <tr>
                <td class="grayLabel" colspan="4">
                    <label for="opportunityDescription "><strong>Description of Opportunity: <em>1. Projects, Roles,
                        Responsibilities. 2. Anticipated Accomplishments</em></strong></label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
					<textarea id="opportunityDescription " name="opportunityDescription" cols="100" rows="2"
                              placeholder="Only the first 100 characters will display in the summary, all will show in the detail." class="form-control required"></textarea>
                </td>
            </tr>
            <tr>
                <td class="grayLabel" colspan="4">
                    <p><strong> Developmental Goals: Please select 2-3 primary Executive Core Qualifications (ECOs) that the
                        participant may cultivate on this assignment.</strong> For more information about ECOs, please visit: <a
                            href="https://www.opm.gov/policy-data-oversight/senior-executive-service/executive-core-qualifications/"
                            target="_blank" title="opens external website in a new window.">https://www.opm.gov/policy-data-oversight/senior-executive-service/executive-core-qualifications/</a>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="grayLabel" colspan="2">
                    <strong><em>
                        ECQs (check all that apply)
                    </em></strong>
                </td>
                <td class="grayLabel" colspan="2">
                    <label for="ECQ_HowOppRelates">Please provide comments about how this assignment relates to the ECQs<BR> and
                        will provide a meaningful work experience for the participant:</label>
                </td>
            </tr>
            <tr>
                <td class="grayLabel">
                    <label for="ECQ_LeadingChange">
                        Leading Change:</label>
                </td>
                <td>
                    <input type="checkbox" name="ECQ_LeadingChange" id="ECQ_LeadingChange" class="form-control" value="1">
                </td>
                <td rowspan="5" colspan="4">
                    <textarea name="ECQ_HowOppRelates" id="ECQ_HowOppRelates" class="form-control" rows="13"></textarea>
                </td>
            </tr>
            <tr>
                <td class="grayLabel">
                    <label for="ECQ_LeadingPeople">
                        Leading People:</label>
                </td>
                <td>
                    <input type="checkbox" name="ECQ_LeadingPeople" id="ECQ_LeadingPeople" class="form-control" value="1">
                </td>
            </tr>
            <tr>
                <td class="grayLabel">
                    <label for="ECQ_ResultsDriven">
                        Results Driven:</label>
                </td>
                <td>
                    <input type="checkbox" name="ECQ_ResultsDriven" id="ECQ_ResultsDriven" class="form-control" value="1">
                </td>
            </tr>
            <tr>
                <td class="grayLabel">
                    <label for="ECQ_BusinessAcumen">
                        Business Acumen:</label>
                </td>
                <td>
                    <input type="checkbox" name="ECQ_BusinessAcumen" id="ECQ_BusinessAcumen" class="form-control" value="1">
                </td>
            </tr>
            <tr>
                <td class="grayLabel">
                    <label for="ECQ_BuildingCoalitions">
                        Building Coalitions:</label>
                </td>
                <td>
                    <input type="checkbox" name="ECQ_BuildingCoalitions" id="ECQ_BuildingCoalitions" class="form-control"
                           value="1">
                </td>
            </tr>
            <tr>
                <td class="grayLabel" colspan="4">The Participant will be offered the following developmental opportunities (check all that apply):</td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp1" name="devOpp1" value="1" class="form-control">
                </td>
                <td colspan="7"><label for="devOpp1">A Senior Executive mentor (this may be the host supervisor</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp2" name="devOpp2" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp2">At least one senior-level shadowing experience </label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp3" name="devOpp3" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp3">A peer-level work/project advisor</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp4" name="devOpp4" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp4">Individual Development Plan and regular check-ins on developmental
                        progress </label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp5" name="devOpp5" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp5">A closing assessment of accomplishments and specific recommendations for continued
                        development </label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp6" name="devOpp6" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp6">Access and exposure to senior-level meetings</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp7" name="devOpp7" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp7">Subject-specific onboarding designed to provide learning on a key skill, issue, profession, etc. </label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp8" name="devOpp8" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp8">Participation in agency-provided training, such as online learning, workshops,
                        speaker series, etc.</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp9" name="devOpp9" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp9">Supervisory experience </label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp10" name="devOpp10" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp10">Cross-agency collaboration experience</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp11" name="devOpp11" value="1" class="form-control">
                </td>
                <td colspan="7">
                    <label for="devOpp11">Project management experience</label>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="checkbox" id="devOpp12" name="devOpp12" value="1" class="form-control">
                </td>
                <td colspan="1">
                    <label for="devOpp12">Other (please explain):</label>
                </td>
                <td colspan="6" bgcolor="##E6F2FB">
                    <textarea id="otherDevOpp" name="otherDevOpp" class="form-control"
                              placeholder="text description of any other development opportunity" >
                    </textarea>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="4" class="grayLabel">
                    <label for="otherDevOpp">How would this opportunity benefit the participant and his/her home organization upon their return? </label>
                </td>
            </tr>
            <tr>
            <td colspan="4">
             <textarea id="howBenefitParticipant" name="howBenefitParticipant" class="form-control"
               placeholder="text description of how opportunity would benefit the participant"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="grayLabel">
                    <label for="specialRequirements">Special Requirements (if any):</label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <textarea name="specialRequirements" id="specialRequirements" class="form-control" placeholder="optional"></textarea>
                </td>
            </tr>
        <tr>
            <td>
                To indicate that you have finished entering the information and are ready to publish this Rotation Opportunity, please type the name as seen in the next box, and then click Submit:
            </td>
            <td colspan="2">
                <label for="hostSupervisorSignature">Host Supervisor's Signature (#session.username#)</label>&nbsp;&nbsp;
                <input type="text" class="form-control" name="hostSupervisorSignature" id="hostSupervisorSignature" placeholder="Type Name Here" required>
                <input type="hidden" id="hostSupervisorSignDate" name="hostSupervisorSignDate" value="#DateFormat(Now(),"yyyy-mm-dd")#"   />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <button type="submit" name="btnSaveRotation" id="btnSaveRotation" value="Submit" class="form-control btn btn-success">
                    <i class="fa fa-check-square" title="Submit" style="margin-right: 5px"></i><strong>Submit</strong>
                </button>
            </td>
            <td colspan="2" align="center">
                <a href="Summary.cfm">
                    <button type="button" class="btn btn-info form-control" style="padding: 5px; font-size: 14px; font-weight: bold; text-decoration: none">
                    <i class="fa fa-times-rectangle" title="Update" style="margin-right: 5px"></i>Cancel</button>
                </a>
            </td>
        </tr>
    </table>
    </fieldset>
</form>
</div>
</cfoutput>

    <script>
        $("#centerOffice").change(function() {
            if (typeof $(this).data('options') === "undefined") {
                /*Taking an array of all options-2 and kind of embedding it on the select1*/
                $(this).data('options', $('#subComponent option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-option=' + id + ']');
            $('#subComponent').html(options);
            $('#subComponent').selectpicker('refresh');
        });
    </script>
<cf_footer>
