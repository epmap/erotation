<cf_header title="eRotation Opportunities">

<cfparam name="URL.showme" default="allRotations"/>
<cfparam name="FORM.fltr_centerOffice" default="0"/>

<cftry>
    <!---TODO: change over to ListOfValues--->
        <cfinvoke component="CFCs/FDAData" method="getActiveCenters" returnvariable="rtn_getActiveCenters">
    <cfcatch>
        <cfif application.environment IS NOT "Development" and application.environment IS NOT "SharedDev">
            <cferror type="exception" exception="any" template="error.cfm">
        <cfelse>
            <h2>Problem fetching centers</h2>
            <cfdump var="#cfcatch#" label="cfcatch" expand="true" abort="false"/>
        </cfif>
    </cfcatch>
</cftry>

    <!---Get all the active Rotations--->
    <cfinvoke component="CFCs.rotationOpps" method="fnc_summaryActiveRotationOpps" returnvariable="rtn_AllRotations">
        <cfinvokeargument name="isActive" value="1">
        <cfif structKeyExists(FORM,"fltr_centerOffice") AND len(FORM.fltr_centerOffice) AND FORM.fltr_centerOffice NEQ 0>
            <cfinvokeargument name="center_ID" value="#FORM.fltr_centerOffice#">
        </cfif>
    </cfinvoke>
    <cfset AllCount = #rtn_AllRotations.recordcount#>

    <!---Get all the active Rotations for the viewing Host Supervisor--->
    <cfif NOT isUserInAnyRole("Employee")>
        <cfinvoke component="CFCs.rotationOpps" method="fnc_RotationOppsBySupervisor" returnvariable="rtn_SupRotations">
            <cfinvokeargument name="isActive" value="1">
            <cfif structKeyExists(FORM,"fltr_centerOffice") AND len(FORM.fltr_centerOffice) AND FORM.fltr_centerOffice NEQ 0>
                <cfinvokeargument name="center_ID" value="#FORM.fltr_centerOffice#">
            </cfif>
            <cfinvokeargument name="hostSupervisorCapHR_ID" value="#session.user_id#">
        </cfinvoke>
        <cfset SupervisorCount = #rtn_SupRotations.recordcount#>
    </cfif>

    <!---Get all the Inactive Rotations for the viewing Host Supervisor--->
    <cfif NOT isUserInAnyRole("Employee")>
            <cfinvoke component="CFCs.rotationOpps" method="fnc_RotationOppsBySupervisor" returnvariable="rtn_SupRotationsInActive">
                    <cfinvokeargument name="isActive" value="0">
                    <cfif structKeyExists(FORM,"fltr_centerOffice") AND len(FORM.fltr_centerOffice) AND FORM.fltr_centerOffice NEQ 0>
                        <cfinvokeargument name="center_ID" value="#FORM.fltr_centerOffice#">
                    </cfif>
                    <cfinvokeargument name="hostSupervisorCapHR_ID" value="#session.user_id#">
            </cfinvoke>
        <cfset SupervisorCount = #rtn_SupRotations.recordcount#>
    </cfif>

    <!---Get all the rotations where user is participant--->
    <cfinvoke component="CFCs.rotationOpps" method="fnc_RotationOppsByParticipant" returnvariable="rtn_MyRotations">
        <cfinvokeargument name="isActive" value="1">
            <cfinvokeargument name="participantCapHR_ID" value="#session.user_id#">
            <cfif structKeyExists(FORM,"fltr_centerOffice") AND len(FORM.fltr_centerOffice) AND FORM.fltr_centerOffice NEQ 0>
                <cfinvokeargument name="center_ID" value="#FORM.fltr_centerOffice#">
            </cfif>
    </cfinvoke>

<body>
    <div class="container-fluid" style="margin: 0px 25px 0px 25px">
        <div class="row text-center" style="margin-bottom: 25px">
            <h2 style="font-weight: 400">FDA&nbsp;Rotation&nbsp;Opportunities&nbsp;Summary</h2>
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-8">
                The FDA Rotations Management Application is  designed as a resource to assist host supervisors in establishing rotation opportunities that respond to mission operations and support agency priorities. The system provides a central location for host supervisors to post rotation opportunities and FDA employees can view and express their interest, as appropriate.
            </div>
            <div class="col-sm-2">&nbsp;</div>
        </div>

        <ul class="nav nav-tabs" id="rotationTab" style="width:100%">
            <li><a href="##allRotations" ID="DisplayAllRotations" Title="All Active Rotations"><i class="fa fa-exchange" style="padding-right: 5px"></i>Active Rotations</a></li>
            <cfif NOT isUserInAnyRole("Employee")>
                <li><a href="##SupRotations" ID="SupervisorRotations" Title="Rotations I Entered"><i class="fa fa-address-book-o" style="padding-right: 5px"></i>Active Host Supervisor Rotations</a></li>
                <li><a href="##SupRotationsInActive" ID="SupervisorRotationsInActive" Title="My Inactive Rotations"><i class="fa fa-address-book" style="padding-right: 5px"></i>Inactive Host Supervisor Rotations</a></li>
            </cfif>
            <li><a href="##MyRotations" ID="ParticipantRotations" Title="Requested Rotations"><i class="fa fa-desktop" style="padding-right: 5px"></i>Rotations Requested</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="allRotations" style="overflow: hidden">
                <cfinclude template="tabs/tab_all_rotations.cfm">
            </div>
            <cfif NOT isUserInAnyRole("Employee")>
                <div class="tab-pane" id="SupRotations" style="overflow: hidden">
                    <cfinclude template="tabs/tab_supervisor_rotations.cfm">
                </div>
                <div class="tab-pane" id="SupRotationsInActive" style="overflow: hidden">
                    <cfinclude template="tabs/tab_supervisor_inactive_rotations.cfm">
                </div>
            </cfif>
            <div class="tab-pane" id="MyRotations" style="overflow: hidden">
                <cfinclude template="tabs/tab_my_rotations.cfm">
            </div>

        </div>

    </div> <!--- END container --->

</body>

<cf_footer>
