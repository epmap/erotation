<style>
table.dataTable td {
    padding: 5px;
}
</style>

<script>
	$(document).ready( function () {
    	$('table.display').DataTable({
			responsive: true,
			stateSave: false,
			order: [[ 0, 'desc' ]],
			"oLanguage": {
  			 "sSearch": "Keywords to refine results"
 			}
    	});
	});
</script>
<!--- TODO - this should be in the customTag --->