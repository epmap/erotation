<cfif isdefined('form.error_comment')>

	<cfmail to="#REQUEST.devEmailGroup#" from="Title 38 <#REQUEST.devEmail#>" subject="#REQUEST.emailTitle# Title 38 Error: Additional Comments" type="html">
		#form.Error_comment#
	</cfmail>

</cfif>

<cf_header title="Error Occurred">

<div class="container">
	<h2>Error</h2>

	<h6>We are so sorry. Something went wrong. We have emailed an error report to the Development team.</h6>
	<br>
	If you would like to send us some additional comments, please use this form.<br>
	<br>
	Thank you.<br>
	<br>

	<form method="post" action="error.cfm?sendComments">
		<div class="page-wrapper box-content">
			Comments: <textarea name="Error_comment" class="content"></textarea>
		</div>
		<div class="text-center">
			<button name="save" class="btn btn-ar btn-info">Send Comments</button>
		</div>
	</form>
</div>

<script>
	$(document).ready(function () {
		$('.content').richText();
	});
</script>

<cfif isdefined('error.message')>
<!--- We can't email the error content if a user is only submitting the form, above --->
	<cfsavecontent variable="errortext">
		<cfoutput>
		An error occurred in the Title38 application in the following environment (#application.environment#): <br>
		Page: http://#cgi.server_name##cgi.script_name#?#cgi.query_string#<BR>
		Time: #dateFormat(now(), "short")# #timeFormat(now(), "short")#<BR>
		<cf_Error_Display>
		</cfoutput>
	</cfsavecontent>

	<cfmail to="#REQUEST.devEmailGroup#" from="Title38 <#REQUEST.devEmail#>" subject="#REQUEST.emailTitle#Title38 Error: #error.message#" type="html">
		#errortext#
	</cfmail>

</cfif>

<cf_footer>