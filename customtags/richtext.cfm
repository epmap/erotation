<cfparam name="attributes.classes" default="content">
<!---
	If you are adding more than one textarea on a page, it must have a unique class name, and it IS Case-SenSiTiVe!

	You can add it to the page request like this:

	EXAMPLE 1:
	<textarea name="PMAP_comment_#cei_section#" 			class =    "content_#cei_section#"    rows="10" cols="120"></textarea>
	<cfset request.RichTextClassesList = request.RichTextClassesList & "content_#cei_section#,">

	EXAMPLE 2:
	<textarea name="PMAP_comment" id="PMAP_comment" 			 class="content_PMAP_comment"></textarea>
	<cfset request.RichTextClassesList = request.RichTextClassesList & "content_PMAP_comment,">

	Notice how the class and appended value are the same!
--->


<script>
	$(document).ready(function() {
		<cfloop list="#attributes.classes#" index="theClass">
		<cfoutput>
			$('.#theClass#').richText(<cfif structKeyExists(application,'RichTextOptions')>{#application.RichTextOptions#}</cfif>)
		</cfoutput>
		</cfloop>
	});
</script>
