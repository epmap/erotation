<cfcomponent displayname="FDA Data" hint="ColdFusion Component for getting FDA data for employees">

	<cffunction access="public" name="getAllOffices" hint="Gets All Offices" returntype="query">

		<cfargument name="officeID" required="no">

		<cfset var getAllOffices = "">
		<cfquery name="getAllOffices" >
			SELECT	FDA_Offices.Office_ID, FDA_Offices.Office_Name, FDA_Offices.office_admin_code,
					FDA_Centers.Center_Name, FDA_Centers.Center_Acronym,  FDA_Offices.center_id
			FROM 	FDA_Offices
			LEFT OUTER JOIN FDA_Centers ON FDA_Offices.CENTER_ID = FDA_Centers.Center_ID
			<cfif structKeyExists(ARGUMENTS,'officeID') AND ARGUMENTS.officeID NEQ 0>
			WHERE 	office_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#TRIM(ARGUMENTS.officeID)#">
			</cfif>
			ORDER BY    CENTER_ACRONYM, Office_Name

		</cfquery>

		<cfreturn getAllOffices />

	</cffunction>

	<cffunction access="public" name="getActiveCenters" hint="Gets All Active Centers" returntype="query">
		<cfargument name="centerID" required="no">
		<cfset var getActiveCenters = "">
		<cfquery name="getActiveCenters" >
			SELECT      Center_ID, Center_Acronym, Center_Name, admin_code, Center_Acronym + ' - ' + Center_Name AS CenterDisplayName
			FROM        FDA_Centers
			WHERE		Center_Active = 1
			<cfif structKeyExists(ARGUMENTS,'centerID')>
				AND Center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#TRIM(ARGUMENTS.centerID)#">
			</cfif>
			ORDER BY 	Center_Acronym
		</cfquery>
		<cfreturn getActiveCenters />
	</cffunction>

	<cffunction access="public" name="qGetRatingOfficials" hint="Gets All Active Rating Officials" returntype="query">

		<cfargument name="currentRO" required="yes" type="numeric" default="0">
		<cfargument name="showRO" required="yes" type="numeric"><!--- Decides if the current RTO is displayed or not --->
		<cfargument name="Center_ID" required="no" type="numeric">

		<cfset var qGetRatingOfficials = "">
		<cfquery name="qGetRatingOfficials" >
			SELECT		Employees.NED_ID, Employees.First_Name, Employees.Last_Name, Employees.Employee_Roles, Employees.last_Name + ', ' + Employees.First_Name as RTO_Name
			FROM		Employees
			WHERE		Employees.isActive = 1
			AND 		Employees.Employee_Roles IN ('Rating_Official','Reviewing_Official','Coordinator')
			<cfif val(ARGUMENTS.showRO) NEQ 0>
					AND (Employees.NED_ID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ARGUMENTS.currentRO#">))
			</cfif>
			<cfif structKeyExists(arguments,'Center_ID') AND val(arguments.Center_ID) GT 0>
				AND Employees.center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(arguments.Center_ID)#">
			<cfelse>
				<cfif structKeyExists(session,'employee_center_id') and val(session.employee_center_id) GT 0>
					AND Employees.center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(session.employee_center_id)#">
				</cfif>
			</cfif>
			ORDER BY Last_name, First_name
		</cfquery>

		<cfreturn qGetRatingOfficials />

	</cffunction>

	<cffunction access="public" name="qGetAllReviewingOfficials" hint="Gets All Active Rviewing Officials" returntype="query">

		<cfset var qGetAllReviewingOfficials = "">
		<cfquery name="qGetAllReviewingOfficials" >
			SELECT Employees.NED_ID,  Employees.First_Name,  Employees.Last_Name,  Employees.Employee_Roles,  Employees.Center_ID
			FROM Employees WITH (NOLOCK)
			WHERE Employees.Employee_Roles = 'Reviewing_Official'
			AND  Employees.isActive = 1
			<cfif NOT isUserInAnyRole('Developer,Admin')>
				AND Employees.Center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(session.Employee_Center_ID)#">
			</cfif>
			ORDER BY Last_Name, First_Name
		</cfquery>

		<cfreturn qGetAllReviewingOfficials />

	</cffunction>


	<cffunction access="public" name="qGetAllPMAPStatuses" hint="Gets All PMAP Statuses" returntype="query">

		<cfset var qGetAllPMAPStatuses = "">
		<cfquery name="qGetAllPMAPStatuses" cachedWithin="#createTimeSpan( 0, 1, 0, 0 )#" >
			SELECT 	PMAP_status_id, PMAP_status_name
			FROM    PMAP_Status
		</cfquery>

		<cfreturn qGetAllPMAPStatuses />

	</cffunction>

    <cffunction access="public" name="qGetSearchablePMAPStatuses" hint="Gets All PMAP Statuses" returntype="query">

	<cfset var qGetSearchablePMAPStatuses = "">
	<cfquery name="qGetSearchablePMAPStatuses" cachedWithin="#createTimeSpan( 0, 1, 0, 0 )#" >
	    SELECT 	PMAP_status_id, PMAP_status_name
	    FROM    PMAP_Status
	    WHERE   PMAP_Status_Type = <cfqueryparam cfsqltype="cf_sql_varchar" value="Current" />
	</cfquery>

	<cfreturn qGetSearchablePMAPStatuses />

    </cffunction>


	<cffunction access="public" name="qGetPMAPSupervisors" hint="Gets PMAP Supervisors - Rating Officials and Reviewing Officials" returntype="query">

		<cfargument name="supervisorID" required="no" type="numeric" default="0">
		<cfargument name="center_ID" required="no" type="numeric" default="0">

		<cfif structKeyExists(session,'Employee_Center_ID')>
			<cfset variables.Center_ID = session.Employee_Center_ID>
		</cfif>
		<cfif structKeyExists(ARGUMENTS,'Center_ID') AND ARGUMENTS.Center_ID GT 0>
			<cfset variables.Center_ID = arugments.Center_ID>
		</cfif>

		<cfset var qGetPMAPSupervisors = "">
		<cfquery name="qGetPMAPSupervisors" >
			SELECT 		Employee_UUID, CapHR_ID, NED_ID, Employee_Roles, First_Name, Last_Name, center_ID,
						last_Name + ', ' + First_Name as RTO_Name
			FROM        Employees WITH (NOLOCK)
			WHERE (Employee_Roles = 'Rating_Official' OR Employee_Roles = 'Reviewing_Official')
				AND isActive = 1
			<cfif val(ARGUMENTS.supervisorID) NEQ 0>
				AND NED_ID = <cfqueryparam value="#numberFormat(ARGUMENTS.supervisorID, "00000000")#" cfsqltype="cf_sql_varchar">
			</cfif>
			<cfif (structKeyExists(arguments,'Center_ID') AND arguments.Center_ID GT 0)>
				AND Employees.center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arugments.Center_ID#">
			<cfelseif structKeyExists(session,'Employee_Center_ID')>
				AND Employees.center_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#val(session.Employee_Center_ID)#">
			</cfif>
			ORDER BY    Last_Name, First_name
		</cfquery>
		<cfreturn qGetPMAPSupervisors />
	</cffunction>

	<cffunction access="public" name="qGetPMAPAdmins" hint="Gets PMAP Administrators" returntype="query">

		<cfset var qGetPMAPAdmins = "">
		<cfquery name="qGetPMAPAdmins" cachedwithin="#createTimeSpan('0','1','0','0')#" >
			SELECT 		CapHR_ID, NED_ID, Employee_Roles, First_Name, Last_Name, email_address
			FROM        Employees
			WHERE       Employee_Roles IN ('Developer,Admin')
			AND			isActive = 1
			ORDER BY 	Last_Name, First_name
		</cfquery>

		<cfreturn qGetPMAPAdmins />

	</cffunction>

</cfcomponent>