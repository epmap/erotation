<cfif NOT isDefined('request.headerComplete')>
	<cf_header title="ePMAP - Application error">
</cfif>
<header id="header-full-top" class="hidden-xs header-full" style="height:1000px">
	<div class="container container-fluid">
		<br>
		<br>
		<div class="alert alert-info text-center" role="alert" style="max-height: 300px">
			<div class="row">
				<div class="col-lg-2 text-right" style="max-height: 150px">
					<span class="fa fa-exclamation-triangle fa-4x"></span>
				</div>
				<div class="col-lg-10 text-left">
					<h2>Error 404 - Page not found</h2>
					<h4>Sorry, this isn't really where you should be. Please try going back.</h4>
				</div>
			</div>
		</div>
	</div>
</header>

<cf_footer>