<cfsilent>

	<cfparam name="Attributes.type"			default = "success">
	<cfparam name="attributes.fade" 		default = 0>
	<cfparam name="attributes.color" 		default = "##73AF55">
	<cfparam name="attributes.message" 		default = "Your changes were saved">
	<cfparam name="attributes.subMessage" 	default = "">
	<cfparam name="attributes.size"			default = "">
	<cfparam name="attributes.IconDisplay"	default = "Yes">
	<cfparam name="attributes.FaIconName"	default = "">
	<cfparam name="attributes.FaIconSize"	default = "">

	<cfparam name="variables.IconType" 		default = "SVG">
	<cfparam name="variables.faIconPadding" default = "">

	<cfif LEN(attributes.FaIconName)>
		<cfset variables.IconType = "FA">
		<cfset variables.FaIconName = attributes.FaIconName>
	</cfif>

	<cfif attributes.size IS "small">
		<cfset variables.messageSize = "h4">
		<cfset variables.SubmessageSize = "h6">
		<cfif attributes.FaIconSize EQ "">
			<cfset attributes.FaIconSize = "fa-2x">
			<cfset variables.faIconPadding = "7px">
		</cfif>
		<cfset variables.DivStyle = "height:50px; padding-top: 8px">
	<cfelse>
		<cfset variables.messageSize = "h2">
		<cfset variables.SubmessageSize = "h4">
		<cfif attributes.FaIconSize EQ "">
			<cfset attributes.FaIconSize = "fa-4x">
		</cfif>
		<cfset variables.DivStyle = "min-height: 100px; max-height: 250px">
	</cfif>

	<cfif Attributes.type IS "Error">
		<cfset Attributes.type = "Danger">
	</cfif>

	<cfif Attributes.type IS "Danger">
		<cfset attributes.color="##D06079">
	<cfelseif Attributes.type IS "Warning">
		<cfset attributes.color="##FFE63B">
	<cfelseif Attributes.type IS "Info">
		<cfset attributes.color="##7699DA">
	</cfif>

	<!--- If more than one checkbox is on the page, it must have a unique ID for the "Fade" functionality to work  --->
	<cfset variables.MSG_UUID = createUUID()>

</cfsilent>

<cfoutput>

	<cfif attributes.fade EQ 1>
		<div id="messageBoxAlert_#variables.MSG_UUID#" class="modal fade" role="dialog" style="bottom: initial!important; text-align: center;">
		<div class="modal-dialog" style="text-align: left; width: 80%;">
	</cfif>

	<div class="container-fluid" style="padding-top: 20px">
		<div class="alert alert-#lcase(Attributes.type)# text-center" role="alert" style="overflow: hidden; <cfif variables.DivStyle NEQ "">#variables.DivStyle#</cfif>">
			<div class="row">
				<cfif attributes.IconDisplay IS "yes">
					<div class="col-lg-2 text-right" style="max-height: 150px">
						<cfif variables.IconType IS "svg">
							<cfif Attributes.type IS "success">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2" <cfif attributes.size EQ "small">width="50%" height="30px"<cfelse> width="100%" height="80px" </cfif> >
									<circle class="path circle" fill="none" stroke="#attributes.color#" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
									<polyline class="path check" fill="none" stroke="#attributes.color#" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
								</svg>
							<cfelseif Attributes.type IS "info">
								<span class="fa fa-hand-o-right #attributes.FaIconSize#"></span>
							<cfelseif Attributes.type IS "warning">
								<span class="fa fa-exclamation-triangle #attributes.FaIconSize#"></span>
							<cfelse>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2" width="100%" height="80px" >
									<circle class="path circle" fill="none" stroke="#attributes.color#" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
									<line class="path line" fill="none" stroke="#attributes.color#" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3" />
									<line class="path line" fill="none" stroke="#attributes.color#" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2" />
								</svg>
							</cfif>
						<cfelseif variables.IconType EQ "FA" AND variables.FaIconName NEQ "">
							<i class="fa #variables.FaIconName# #attributes.faIconSize#" style="padding-top: #variables.faIconPadding#"></i>
						</cfif>
					</div>
				</cfif>
				<cfif attributes.IconDisplay IS "yes">
					<div class="col-lg-10 text-left" style="overflow: hidden">
				<cfelse>
					<div class="col-lg-12 text-left" style="overflow: hidden">
				</cfif>
					<cfif LEN(trim(attributes.message))><#variables.messageSize#>#attributes.message#</#variables.messageSize#></cfif>
					<cfif LEN(trim(attributes.Submessage))><#variables.SubmessageSize#>#attributes.submessage#</#variables.SubmessageSize#></cfif>
				</div>
			</div>
		</div>
	</div>

	<cfif attributes.fade EQ 1>
			</div>
		</div>

		<script>
			$(document).ready(function(){
			$('##messageBoxAlert_#variables.MSG_UUID#').appendTo("body").modal('show');
			$("##messageBoxAlert_#variables.MSG_UUID#").delay(1500).fadeOut(1500);
		});
		</script>
	</cfif>

	
</cfoutput>