--Add missing column to hold Signature input
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[RotationOpp]') 
         AND name = 'hostSupervisorSignature'
		 )
alter table RotationOpp
	add hostSupervisorSignature varchar(80)
go