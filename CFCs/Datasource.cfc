<cfcomponent displayname="Datasource" output="false">

	<cfproperty name="DSName" type="string" default=""/>

	<cfset VARIABLES.instance = {
		DSName = ''
	}/>

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="DSName" required="true" type="String"/>
		<cfscript>
			VARIABLES.instance.DSName = arguments.DSName;
		</cfscript>
		<cfreturn this/>
	</cffunction>

	<cffunction name="getDSName" access="public" output="false">
		<cfreturn VARIABLES.instance.DSName/>
	</cffunction>

</cfcomponent>