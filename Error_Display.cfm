<style>
    .gray {
        background-color: #d6dde3;
    }
</style>

<cfoutput>

    <table class="table table-striped">
        <tr>
            <td colspan="2" class="gray">ERROR VARIABLES</td>
        </tr>
	<cfif isdefined('error.message')>
		<cfloop item="x" collection="#ERROR#">
                <tr>
                <td>ERROR.#x#</td>
            <td>#ERROR[x]#</td>
            </tr>
		</cfloop>
            <tr>
                <td colspan="2" height="30"></td>
            </tr>
	<cfelse>
            <tr>
                <td colspan="2" height="25">NO VARIABLES AVAILABLE</td>
            </tr>
            <tr>
                <td colspan="2" height="30">&nbsp;</td>
            </tr>
	</cfif>
<!--- START FORM DISPLAY--->

        <tr>
            <td colspan="2" class="gray">FORM VARIABLES</td>
        </tr>
	<cfif structCount(#FORM#) GT 0>
		<cfloop item="i" collection="#FORM#">
                <tr>
                <td height="25">FORM.#i#</td>
            <td>#FORM[i]#</td>
            </tr>
		</cfloop>
	<cfelse>
            <tr>
                <td colspan="2" height="25">NO FORM VARIABLES AVAILABLE</td>
            </tr>
	</cfif>
        <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
<!--- START URL DISPLAY--->
        <tr>
            <td colspan="2" class="gray">URL VARIABLES</td>
        </tr>
	<cfif structCount(#URL#) GT 0>
		<cfloop item="u" collection="#URL#">
                <tr>
                <td height="25">URL.#u#</td>
            <td>#URL[u]#</td>
            </tr>
		</cfloop>
	<cfelse>
            <tr>
                <td colspan="2" height="25">NO URL VARIABLES AVAILABLE</td>
            </tr>
	</cfif>
        <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
<!--- START CGI DISPLAY--->
        <tr>
            <td colspan="2" class="gray">CGI VARIABLES</td>
        </tr>
	<cfloop item="c" collection="#CGI#">
            <tr>
            <td height="25" width="300">CGI.#c#</td>
        <td>#CGI[c]#</td>
        </tr>
	</cfloop>
        <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
<!--- START SESSION DISPLAY--->
        <tr>
            <td colspan="2" class="gray">SESSION VARIABLES</td>
        </tr>
	<cfif structCount(#SESSION#) GT 0>
		<cfloop item="s" collection="#SESSION#">
                <tr>
                <td height="25">SESSION.#s#</td>
            <td>#SESSION[s]#</td>
            </tr>
		</cfloop>
	<cfelse>
            <tr>
                <td colspan="2" height="25">NO SESSION VARIABLES AVAILABLE</td>
            </tr>
	</cfif>
    </table>



</cfoutput>