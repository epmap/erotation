<!---
    Stop the APPLICATION. After calling this method, the next
    page request to the APPLICATION should start it up again
    (resetting it).
--->
<cfscript>
    structClear(APPLICATION);
    structClear(SESSION);
    applicationStop();
</cfscript>

<!--- Redirect back to overview. --->

<cfif isDefined('URL.resetBaseURL')>
	<cflocation URL="index.cfm?reset_successful&resetBaseURL" addtoken="no">
<cfelse>
	<cflocation URL="index.cfm?reset_successful" addtoken="false"/>
</cfif>