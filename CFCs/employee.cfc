<cfcomponent displayname="FDA Employee Profile Processes" hint="ColdFusion Component for processing Employee Profiles">

	<cffunction name="qGetEmployeeData" access="public" hint="Gets Employee for selected ned_id" returntype="query">

		<cfargument name="UUID" required="yes" type="string">

		<cfset var qGetEmployeeData = "">
		<cfquery name="qGetEmployeeData" >
			SELECT 	CASE WHEN Employees.CapHR_ID IS NULL THEN Employees.NED_ID WHEN Employees.CapHR_ID = '' THEN Employees.NED_ID ELSE Employees.CapHR_ID END AS CAPHR_ID,
			Employees.NED_ID, Employees.fda_username, Employees.Name, Employees.First_Name, Employees.Last_Name,
			Employees.Employee_UUID, Employees.Reports_To, Employees.Display_Name, Employees.Job_Code, Employees.email_address,
			Employees.Phone, Employees.Employee_Roles, Employees.Job_Title, Employees.office_id, Employees.center_ID, Employees.Location,
			Employees.DeptID, Employees.isCommissionedCorps, Employees.isExecutive, Employees.employee_notes, Employees.Pay_Plan,
			Employees.Occ_Series, Employees.Step, Employees.Grade, Employee_Roles.Role_Name, Employee_Roles.Role_ID,
			FDA_Centers.Center_Name, FDA_Centers.Center_Acronym, FDA_Centers.Center_ID, FDA_Offices.office_name,
			Supervisors.First_Name + ' ' + Supervisors.Last_Name AS Supervisor, Supervisors.email_address AS Supervisor_email,
			Supervisors.Phone AS Supervisor_Phone, Supervisor_Center.Center_Acronym AS Supervisor_Center_Acronym,
			Supervisors.NED_ID AS SUPERVISORS_Ned_Id, Supervisor_Center.Center_Name AS Supervisor_Center_Name,
			Employees_To_Roles.EmpRole_ID, Employees_To_Roles.isActive
			FROM   	FDA_Offices RIGHT OUTER JOIN
			Employees ON FDA_Offices.office_id = Employees.office_id LEFT OUTER JOIN
			FDA_Centers AS Supervisor_Center INNER JOIN
			Employees AS Supervisors ON Supervisor_Center.Center_ID = Supervisors.center_ID ON Employees.Reports_To = Supervisors.NED_ID LEFT OUTER JOIN
			FDA_Centers ON FDA_Centers.Center_ID = Employees.center_ID LEFT OUTER JOIN
			Employees_To_Roles INNER JOIN
			Employee_Roles ON Employees_To_Roles.Role_ID = Employee_Roles.Role_ID ON Employees.CapHR_ID = Employees_To_Roles.CapHR_ID
			WHERE  (Employees_To_Roles.App_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#application.app_id#"> OR Employees_To_Roles.App_id IS NULL) AND
		Employees.Employee_UUID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#TRIM(ARGUMENTS.UUID)#">
		</cfquery>

		<cfreturn qGetEmployeeData>

	</cffunction>

	<cffunction access="public" name="updateEmployeeProfile" hint="Update Employee Profile Data">

		<cfset var qUpdateProfile = "">
		<cfquery name="qUpdateProfile">
			<cfif NOT LEN(FORM.App2RoleID)>
				INSERT INTO Employees_To_Roles
				(CapHR_ID, App_id, Role_ID, isActive)
				VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#TRIM(FORM.empID)#">,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#application.app_id#">,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#FORM.employee_role#">,
				<cfqueryparam cfsqltype="cf_sql_integer" value="1">)
			<cfelse>
				UPDATE	Employees_To_Roles
				SET 	Role_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#FORM.employee_role#">,
			isActive = <cfqueryparam cfsqltype="cf_sql_integer" value="#FORM.accountStatus#">
				WHERE 	EmpRole_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#FORM.App2RoleID#">
			</cfif>
		</cfquery>

		<cfset variables.updateStatus = ''>
		<cfset variables.updateRole = ''>

		<cfif structKeyExists(FORM, 'accountStatus') AND FORM.cur_accountStatus NEQ FORM.accountStatus>
			<cfif FORM.accountStatus EQ 1 AND FORM.cur_accountStatus NEQ 1>
				<cfset variables.updateStatus = listAppend(variables.updateStatus, session.username & ' activated the user account for ' & FORM.empName &'.')>
				<cfelseif FORM.accountStatus EQ 0 AND FORM.cur_accountStatus NEQ 0>
				<cfset variables.updateStatus = listAppend(variables.updateStatus, session.username & ' deactivated the user account for ' & FORM.empName&'.')>
			</cfif>
		</cfif>

		<cfif structKeyExists(FORM, 'employee_role') AND FORM.cur_employee_role NEQ FORM.employee_role>
			<cfset var getRoleName = "">
			<cfquery name="getRoleName">
				SELECT 		Role_ID, Role_Name
				FROM   		Employee_Roles
				WHERE  		Role_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#FORM.employee_role#">
			</cfquery>
			<cfif NOT LEN(FORM.cur_employee_role)>
				<cfset variables.updateRole = listAppend(variables.updateRole, FORM.empName & "'s " & "employee role was updated to " & getRoleName.Role_Name & " by " & session.username & ".")>
			<cfelse>
				<cfset variables.updateRole = listAppend(variables.updateRole, FORM.empName & "'s " & "employee role was changed from " & FORM.cur_employee_role_name & " to " & getRoleName.Role_Name & " by " & session.username & ".")>
			</cfif>
		</cfif>

		<!--- Update Profile Log --->
		<cfif LEN(variables.updateStatus)>
			<cfset var qEmployeeProfileComment = "">
			<cfquery name="qEmployeeProfileComment">
				INSERT INTO Employee_Profile_Log
				(app_ID, CapHR_ID, log_message, log_date)
				VALUES
				(
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#application.app_id#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.empID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#TRIM(variables.updateStatus)#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
				)
			</cfquery>
		</cfif>

		<cfif LEN(variables.updateRole)>
			<cfset var qEmployeeProfileComment = "">
			<cfquery name="qEmployeeProfileComment">
				INSERT INTO Employee_Profile_Log
				(app_ID, CapHR_ID, log_message, log_date)
				VALUES
				(
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#application.app_id#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.empID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#TRIM(variables.updateRole)#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
				)
			</cfquery>
		</cfif>

	</cffunction>

	<cffunction access="public" name="getAppRoles" returntype="query" hint="I get all the roles to display in the profile.cfm page">

		<cfset var qGetAppRoles = "">
		<cfquery name="qGetAppRoles">
		SELECT 		Role_ID, Role_Name, App_id, isActive
		FROM   		Employee_Roles
		WHERE  		App_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#application.app_id#"> AND
				 	isActive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
		ORDER BY 	App_id, Role_Name
		</cfquery>

		<cfreturn qGetAppRoles />

	</cffunction>

</cfcomponent>
