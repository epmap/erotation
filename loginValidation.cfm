<cfset variables.RefreshLogin = 0>

<cfif ( NOT isUserLoggedIn() or NOT isDefined('session.user_ID') OR NOT structKeyExists(session,'authenticated') OR NOT structKeyExists(session,'FDA_USERNAME'))>
	<!--- If the relevant required authentication info is missing, check them again --->
	<cfset variables.RefreshLogin = 1>
</cfif>

<cfif FindNoCase('?&update', cgi.http_url)>
	<!--- If they have updated their own profile, refresh their session --->
	<cfset variables.RefreshLogin = 1>
</cfif>

<cfif structKeyExists(session,'employee_roles') AND session.employee_roles IS NOT GetUserRoles()>
	<!--- If the CFLOGIN scope has expired, but the session has not (which happens frequently and is mostly an issue for Admins and Developers operating under proxy sessions) we refresh them
	    BUT THIS WILL BREAK PROXY LOGINS - you will go back to being an adnin. --->
	<cfset variables.RefreshLogin = 1>
</cfif>

<cfif (NOT listLast(cgi.SCRIPT_NAME, "/") IS "login.cfm" AND NOT listLast(cgi.SCRIPT_NAME, "/") IS "logout.cfm") AND variables.RefreshLogin EQ 1>
	<!--- If they are not logging in or out, or creating a new account, refresh their info --->

	<cf_login noHeader="true" noRedirect="true">
	<div style="height: 2px; background-color: dimgray" title="Refreshed expired session"></div>

</cfif>
