<cfoutput>

    <table class="table table-striped">
        <tr>
            <td colspan="2" bgcolor="##fffacd">ERROR VARIABLES</td>
        </tr>
        <cfif structKeyExists(error,'message')>
        <cfloop item="x" collection="#ERROR#">
        <tr>
            <td>ERROR.#x#</td>
            <td>#ERROR[x]#</td>
        </tr>
        </cfloop>
         <tr>
            <td colspan="2" height="30"></td>
        </tr>
       <cfelse>
        <tr>
            <td colspan="2" height="25">NO VARIABLES AVAILABLE</td>
        </tr>
         <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
       </cfif>
       <!--- START FORM DISPLAY--->
        <tr>
            <td colspan="2" bgcolor="##fffacd">FORM VARIABLES</td>
        </tr>
        <cfloop item="i" collection="#FORM#">
        <tr>
            <td height="25">FORM.#i#</td>
            <td>#FORM[i]#</td>
        </tr>
        </cfloop>
         <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
       <!--- START URL DISPLAY--->
        <tr>
            <td colspan="2" bgcolor="##fffacd">URL VARIABLES</td>
        </tr>
        <cfloop item="u" collection="#URL#">
        <tr>
            <td height="25">URL.#u#</td>
            <td>#URL[u]#</td>
        </tr>
        </cfloop>
         <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
       <!--- START CGI DISPLAY--->
        <tr>
            <td colspan="2" bgcolor="##fffacd">CGI VARIABLES</td>
        </tr>
        <cfloop item="c" collection="#CGI#">
        <tr>
            <td height="25" width="300">CGI.#c#</td>
            <td>#CGI[c]#</td>
        </tr>
        </cfloop>
         <tr>
            <td colspan="2" height="30">&nbsp;</td>
        </tr>
       <!--- START SESSION DISPLAY--->
        <tr>
            <td colspan="2" bgcolor="##fffacd">SESSION VARIABLES</td>
        </tr>
        <cfloop item="s" collection="#SESSION#">
        <tr>
            <td height="25">SESSION.#s#</td>
            <td>#SESSION[s]#</td>
        </tr>
        </cfloop>
    </table>



</cfoutput>