 <!-- footer-widgets -->

	<script src="../assets/js/app.js"></script>
	<script src="../assets/js/home_info.js"></script>


	<footer id="footer" class="footer-widgets">
		<p style="padding: 5px">eRotation Version 1.0.0 | April 17, 2020</p>
		<p style="padding: 5px">FDA Official: <a href="mailto:Fernandez.Vines@fda.hhs.gov">Corey Vines</a> | Program Manager: <a href="mailto:Vanessa.Watters@fda.hhs.gov">Vanessa Watters</a>
		<p style="padding: 5px">Technical Support: <a href="mailto:OO-OHCM-eRotation@fda.hhs.gov">OO-OHCM-eRotation@fda.hhs.gov</a></p>
	</footer>

</body>

</html>

 <script>
	 $(document).ready(function () {
		 $('#rotationTab a').click(function (e) {
			 e.preventDefault();
			 $(this).tab('show');
		 });

		 // store the currently selected tab in the hash value
		 $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
			 var id = $(e.target).attr("href").substr(1);
			 $.cookie('activeTab', id);
		 });

		 // on load of the page: switch to the currently selected tab
		 var hash = $.cookie('activeTab');
		 if (hash != null) {
			 $('#rotationTab a[href="#' + hash + '"]').tab('show');
		 }

		 $(function() {

			 // Enable on selected forms
			 $('form.dirty-check').areYouSure();

		 });

		 var panels=localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels); //get all panels
		 for (var i in panels){ //<-- panel is the name of the cookie
			 if ($("#"+panels[i]).hasClass('panel-collapse')) // check if this is a panel
			 {
				 $("#"+panels[i]).collapse("show");
			 }
		 }

		 $(".panel .panel-collapse").on('shown.bs.collapse', function ()
		 {
			 var active = $(this).attr('id');
			 var panels= localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
			 if ($.inArray(active,panels)==-1) //check that the element is not in the array
				 panels.push(active);
			 localStorage.panels=JSON.stringify(panels);
		 });

		 $(".panel .panel-collapse").on('hidden.bs.collapse', function ()
		 {
			 var active = $(this).attr('id');
			 var panels= localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
			 var elementIndex=$.inArray(active,panels);
			 if (elementIndex!==-1) //check the array
			 {
				 panels.splice(elementIndex,1); //remove item from array
			 }
			 localStorage.panels=JSON.stringify(panels); //save array on localStorage
		 });

	 });
 </script>