
<div class="row" style="margin-bottom: 10px">

    <div class="col-md-6">
        <cfif NOT isUserInAnyRole("Employee")>
            <cfoutput>
                <a href="#application.baseURL#/Auth/RotationOpportunityEntryForm.cfm">
                <button type="submit" name="btnAddNewRotation" id="btnAddNewRotation" value="Submit" class="btn btn-default navbar-btn relocright">
                    Add Rotation Opportunity
                </button>
            </a>
            </cfoutput>
        </cfif>
    </div>
    <div class="col-md-6">
        <cfoutput>
            <form action="Summary.cfm?#cgi.query_string#" method="post">
                <label for="fltr_centerOffice">Filter by Center</label>
                <select name="fltr_centerOffice" id="fltr_centerOffice" class="form-control" data-live-search="true" data-size="10" data-dropup-auto="false" title="Select center, click Refresh" onchange="$(this.form).trigger('submit')">
                    <option value="0">All Centers</option>
                <cfloop query="rtn_getActiveCenters">
                        <option value="#rtn_getActiveCenters.Center_ID#" <cfif FORM.fltr_centerOffice EQ rtn_getActiveCenters.Center_ID>selected</cfif>>#rtn_getActiveCenters.Center_Acronym#</option>
                </cfloop>
                </select>
            </form>
        </cfoutput>
    </div>
</div>

<cfif rtn_AllRotations.recordcount GTE 1>

    <table class="display table table-striped table-responsive small" id="AllRotations">
        <thead>
            <tr class="success" role="row">
                <th>&nbsp;</th>
                <th align="center">OPP&nbsp;#</th>
                <th>HOST SUPERVISOR</th>
                <th>CENTER/OFFICE</th>
                <th>OPPORTUNITY TITLE</th>
                <th width="50%">DESCRIPTION</th>
                <th>CLEARANCE</th>
                <th>PARTICIPANT LOCATION</th>
            </tr>
        </thead>
        <tbody>
        <cfoutput Query="rtn_AllRotations">

            <cfquery name="qry_GetRotationCenter" >
                SELECT  Center_Name, Center_Acronym
                FROM    FDA_Centers
                WHERE   Center_ID = <cfqueryparam value="#rtn_AllRotations.centerOffice#" cfsqltype="cf_sql_integer">
            </cfquery>


        <tr style="min-height: 80px; text-transform: capitalize; color: black">
                <td>
                    <button class="btn btn-info text-center" onclick="window.location='RotationOpportunityDetail.cfm?rotOpp_ID=#rotOpp_ID#'"><i class="fa fa-search"></i>
                    </button>
                </td>
                <td align="center">#rotOpp_ID#</td>
                <td>#hostSupervisorName#</td>
                <td>#qry_GetRotationCenter.center_Acronym#</td>
                <td>#rotationTitle#</td>
                <td>#justification#</td>
                <td>#clearencesRequired#</td>
                <td>#participantLocation#</td>
            </tr>
        </cfoutput>
    </table>
<cfelse>
    <h5 style="font-weight: 400">There are currently no rotation opportunities available. Please check back later.</h5>
</cfif>