<cfinclude template="../header.cfm">

<cfparam name="URL.emp_fname" default="" />
<cfparam name="URL.emp_lname" default="" />
<cfparam name="URL.center" default="0" />
<cfparam name="URL.emp_job" default="" />
<cfparam name="URL.emp_role" default="" />

<cfinvoke component="CFCs/FDAData" method="getActiveCenters" returnvariable="rtn_getActiveCenters">

<cfif structKeyExists(FORM,'searchPage')>

    <cfset session.queryString = '?searchType=Employees&emp_fname='&#FORM.s_firstName#&'&emp_lname='&#FORM.s_lastName#&'&center='&#FORM.s_center#>
    <cfif FORM.searchPage IS 'Employees'>
        <cflocation url="manage_users.cfm?searchType=Employees&emp_fname=#FORM.s_firstName#&emp_lname=#FORM.s_lastName#&center=#FORM.s_center#" addtoken="no">
    </cfif>

</cfif>

<!--- Populates tab_dashboard_search_employees.cfm --->
<cfif structKeyExists(url,'searchType') AND LEN((trim(url.searchType)))>
    <cfinvoke
        component="CFCs/dashboard"
        method="qGetDashboardEmployees"
        user_id="#numberFormat(session.user_id, "00000000")#"
        first_name="#URL.emp_fname#"
        last_name="#URL.emp_lname#"
        center="#val(URL.center)#"
        returnvariable="rtn_qGetDashboardEmployees">
</cfif>

<div class="container-fluid" style="margin: 0px 25px 0px 25px">
    <div class="row">
        <h2 style="font-weight: 400">Manage eRotation Users</h2>
        Use the control panel below to search for employees.
    </div>


    <div class="row" style="margin-top: 15px">
        <cfoutput>
            <form method="post" action="manage_users.cfm" name="EmployeeSearchForm">
                <table class="display table table-striped table-responsive small">
                    <tr>
                        <td>
                            <input type="text" class="form-control" name="s_firstName" Placeholder="First Name" value="#URL.emp_fname#">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="s_lastName" placeholder="Last name" value="#URL.emp_lname#">
                        </td>
                        <td>
                        <select name="s_center" id="s_center" class="form-control selectpicker" data-size="10" data-dropup-auto="true" data-container="body" Title="All Centers">
                            <option value="">All Centers</option>
                            <cfloop query="rtn_getActiveCenters">
                                <option value="#rtn_getActiveCenters.Center_ID#" <cfif URL.Center EQ rtn_getActiveCenters.Center_ID>selected</cfif>>#rtn_getActiveCenters.Center_Acronym#</option>
                                <cfif URL.Center EQ rtn_getActiveCenters.Center_ID><cfset variables.ActiveCenterAcronym = rtn_getActiveCenters.Center_Acronym></cfif>
                            </cfloop>
                        </select>
                        </td>
                        <td>
                            <!---<select name="s_supervisor" class="form-control selectpicker" data-live-search="true" data-size="25" data-dropup-auto="false" data-container="body" title="#variables.ActiveCenterAcronym# Rating Officials" <CFIF val(url.center) EQ 0 AND NOT IsUserInAnyRole('PMAP_Coordinator')>disabled</cfif>>
                                <option value="">All Rating Officials</option><cfloop query="rtn_qGetPMAPSupervisors">
                                <option value="#rtn_qGetPMAPSupervisors.CapHR_ID#" <cfif structkeyexists(URL, 'supervisor') AND (URL.supervisor EQ rtn_qGetPMAPSupervisors.CapHR_ID)>selected</cfif>>#rtn_qGetPMAPSupervisors.RTO_Name#</option></cfloop>
                            </select>--->
                        </td>
                        <td>
                            <!---<select name="s_role" class="form-control selectpicker" data-dropup-auto="false" data-container="body" title="User role">
                                <option value="All" <cfif StructKeyExists(url,'emp_role') AND NOT LEN(url.emp_role)>SELECTED</cfif> >All user roles</option>
                                <option value="Employee" <cfif StructKeyExists(url,'emp_role') AND (url.emp_role) IS "Employee">SELECTED</cfif> >Employee</option>
                                <option value="Rating_Official" <cfif StructKeyExists(url,'emp_role') AND (url.emp_role) IS "Rating_Official">SELECTED</cfif> >Rating Official</option>
                                <option value="Reviewing_Official" <cfif StructKeyExists(url,'emp_role') AND (url.emp_role) IS "Reviewing_Official">SELECTED</cfif> >Reviewing Official</option>
                                <option value="PMAP_Coordinator" <cfif StructKeyExists(url,'emp_role') AND (url.emp_role) IS "PMAP_Coordinator">SELECTED</cfif> >PMAP Coordinator</option>
                            </select>--->
                        </td>
                       <td NOWRAP>
                            <button type="submit" class="btn btn-success" style="width:48%">Search</button>
                            <a href="manage_users.cfm"><button type="button" class="btn btn-warning" style="width:48%; color: black"><i class="fa fa-refresh" style="margin-right: 3px"></i> Reset </button></a>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="searchPage" value="Employees">
                <input type="hidden" name="s_center" value="">
            </form>
        </cfoutput>
    </div>

    <!--- Table Display for employee search --->
    <div class="row">
        <cfif NOT isDefined('rtn_qGetDashboardEmployees.recordcount')>
            <h3 class="text-center"> Please enter your search criteria.</h3>
        <cfelseif rtn_qGetDashboardEmployees.recordcount EQ 0>
            <cf_checkmark type="Warning" size="small" message="Sorry, no matches found.  Please refine your criteria and try again.">
        <cfelseif rtn_qGetDashboardEmployees.recordcount GT 0>

            <cfparam name="URL.startRow" default="1" />
            <cfoutput>
                <h2 class="em-primary-inverse text-center" style="color: white; padding: 5px">
                <cfif rtn_qGetDashboardEmployees.RecordCount GTE request.maxRowsToQuery>More than</cfif> #rtn_qGetDashboardEmployees.RecordCount# Results Found</h2>
                <br>
            </cfoutput>

            <!--- Display pagination if recordcount is greater than 100 (request.maxRowsToDisplay) --->
            <cfif rtn_qGetDashboardEmployees.recordcount GTE request.maxRowsToDisplay>
                <cfset variables.perPage = request.maxRowsToDisplay />
                <cfset variables.totalPages = ceiling(rtn_qGetDashboardEmployees.recordcount / variables.perPage)/>
                <cfset variables.thisPage = ceiling(URL.startRow / variables.perPage)/>
                <cfset variables.lastPage = ceiling(variables.perPage * (variables.totalPages - 1)) + 1/>
                <cfset variables.pastLink = URL.startRow - variables.perPage/>
                <cfif  variables.pastLink LTE 0>
                    <cfset variables.pastLink = 1>
                </cfif>
                <cfset variables.displayLinkEnd = 10/>

                <cfif variables.totalPages GT 10>
                    <cfif StructKeyExists(variables,'thisPage') AND variables.thisPage GTE 10>
                        <cfset variables.displayLinkStart = variables.thisPage - 5/>
                        <cfset variables.displayLinkEnd = variables.thisPage + 4/>
                    <cfelse>
                        <cfset variables.displayLinkStart = 1/>
                        <cfset variables.displayLinkEnd = 10/>
                    </cfif>
                <cfelse>
                    <cfset variables.displayLinkStart = 1/>
                    <cfset variables.displayLinkEnd = variables.totalPages/>
                </cfif>

                <cfoutput>
                    <table id="paginationTop" class="display table table-responsive small">
                        <tr>
                            <td style="padding:5px">
                                <div class="row">
                                    <div class="col-lg-2 text-left">
                                        <cfif structKeyExists(variables,'thisPage') AND variables.thisPage GT 1>
                                            <a href="manage_users.cfm?searchType=Employees&startRow=#variables.pastLink#&emp_fname=#URL.emp_fname#&emp_lname=#URL.emp_lname#&center=#URL.center#"><button class="btn btn-info">Prev #variables.perPage#</button></a>
                                        <cfelse>
                                            <button class="btn .btn-default" DISABLED>Prev</button>
                                        </cfif>
                                    </div>
                                    <div class="col-lg-8 text-center">
                                        <cfif variables.pastLink GT 0>
                                            <h5 style="color:black; font-weight: bold"> &middot;
                                                <cfloop from="#variables.displayLinkStart#" to="#variables.displayLinkEnd#" index="x">
                                                    <cfset variables.pageRowTop = (x * variables.perPage) - 99/>
                                                    <cfif URL.startRow NEQ variables.pageRowTop AND URL.startRow GT 0>
                                                        <a href="manage_users.cfm?searchType=Employees&startRow=#variables.pageRowTop#&emp_fname=#URL.emp_fname#&emp_lname=#URL.emp_lname#&center=#URL.center#" style="text-decoration: underline; padding: 0px 5px 0px 5px">#variables.pageRowTop# - #x#00</a>
                                                    <cfelse>
                                                        <span style="width: 120px; color: black; padding: 0px 5px 0px 5px">#variables.pageRowTop# - #x#00</span>
                                                    </cfif> &middot;
                                                </cfloop>
                                            </h5>
                                        </cfif>
                                    </div>
                                    <div class="col-lg-2 text-right">
                                        <cfif (URL.startRow + variables.perPage - 1) LT rtn_qGetDashboardEmployees.recordcount>
                                            <cfset variables.nextLink = URL.startRow + variables.perPage/>
                                            <a href="manage_users.cfm?searchType=Employees&startRow=#variables.nextLink#&emp_fname=#URL.emp_fname#&emp_lname=#URL.emp_lname#&center=#URL.center#"><button class="btn btn-info">Next #variables.perPage#</button></a>
                                        <cfelse>
                                            <button class="btn .btn-default" DISABLED>Next</button>
                                        </cfif>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </cfoutput>
            </cfif>

            <!--- Display Table Header --->
            <table class="table table-striped table-responsive small employeeData">
                <thead>
                    <tr class="info">
                        <th>&nbsp;</th>
                        <th>Active</th>
                        <th>Employee ID</th>
                        <th NOWRAP>Employee</th>
                        <th>eRotation Role</th>
                        <th>Center-Office/Sub-Office</th>
                        <th>Supervisor</th>
                    </tr>
                </thead>
                <tbody>
                <cfoutput query="rtn_qGetDashboardEmployees" startrow="#URL.startRow#" maxrows="#request.maxRowsToDisplay#">
                    <tr>
                        <td>
                            <a href="#application.baseURL#/auth/profile.cfm?action=edit&employeeID=#TRIM(rtn_qGetDashboardEmployees.Employee_UUID)#">
                                <button class="btn btn-info" style="padding: 5px">
                                    <i class="fa fa-user-circle" title="Update" style="margin-right: 3px"></i> <span style="font-size: 13px"> Update Employee</span>
                                </button>
                            </a>
                        </td>
                        <td>
                            <cfif rtn_qGetDashboardEmployees.isActive EQ 1>
                                <i class="fa fa-toggle-on fa-2x"></i>
                            <cfelse>
                                <i class="fa fa-toggle-off fa-2x"></i>
                            </cfif>
                        </td>
                        <td>
                            #numberFormat(rtn_qGetDashboardEmployees.CapHR_ID, "00000000")#
                        </td>
                        <td>
                            <b style="font-weight: bolder; text-transform: Capitalize; font-size: 12px">#ucase(rtn_qGetDashboardEmployees.last_name)#, #ucase(rtn_qGetDashboardEmployees.first_name)#</b>
                            <a href="mailto:#rtn_qGetDashboardEmployees.email_address#" title="EMAIL #ucase(rtn_qGetDashboardEmployees.first_name)#"><i class="fa fa-envelope" style="margin-left: 3px"></i></a>
                            <cfif LEN(rtn_qGetDashboardEmployees.Job_Title)><br>#rtn_qGetDashboardEmployees.Job_Title#</cfif>
                        </td>
                        <td <cfif rtn_qGetDashboardEmployees.role_name EQ 'No Role'> style="color: red; font-weight: bold" </cfif>>
                             #rtn_qGetDashboardEmployees.role_name#
                        </td>
                        <td>
                            <cfif LEN(rtn_qGetDashboardEmployees.Center_Name)>
                                #rtn_qGetDashboardEmployees.Center_Name# (#rtn_qGetDashboardEmployees.Center_Acronym#)
                            <cfelse>
                                <span style="color: red; font-weight: bold">No Assigned Center/Office</span>
                            </cfif>
                            <cfif LEN(rtn_qGetDashboardEmployees.office_name)><br>#rtn_qGetDashboardEmployees.office_name#</cfif>
                        </td>
                        <td>
                            <cfif LEN(trim(rtn_qGetDashboardEmployees.Supervisor))>
                                <a href="mailto:#rtn_qGetDashboardEmployees.Supervisor_email#"><i class="fa fa-envelope" title="EMAIL #ucase(rtn_qGetDashboardEmployees.Supervisor)#" style="margin-right: 3px"></i></a>
                                <b style="text-transform: capitalize">#ucase(rtn_qGetDashboardEmployees.Supervisor)#</b>
                                <cfif LEN(trim(rtn_qGetDashboardEmployees.Supervisor_Center_Acronym))>(#rtn_qGetDashboardEmployees.Supervisor_Center_Acronym#)</cfif><br>
                                <cfif LEN(trim(rtn_qGetDashboardEmployees.Supervisor_Job_Title))>#rtn_qGetDashboardEmployees.Supervisor_Job_Title#</cfif>
                            <cfelse>
                                <span style="color: red; font-weight: bold">No Assigned Supervisor</span>
                            </cfif>
                        </td>
                    </tr>
                </cfoutput>
                </tbody>
            </table>

        </cfif>
    </div>

</div>

    <script>
        $(document).ready( function () {
            $('table.employeeData').DataTable({
                oLanguage: {
                    "sSearch": "Filter within Results:"
                },
                dom: '<"row"><"col-lg-4"l><"col-lg-4"f><"col-lg-4"p><"col-lg-12"t><"row"><"col-lg-4"l><"col-lg-4"f><"col-lg-4"p>',
                stateSave: true,
                info: false,
                ordering: true,
                paging: true,
                bFilter: true,
                bAutoWidth: false,
                pageLength: 25
            });
        } );
    </script>


    <br><br>
<cfinclude template="../footer.cfm">