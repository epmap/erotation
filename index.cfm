<cfif structKeyExists(session,'authenticated') and session.authenticated EQ 1>
	<cflocation url="auth/Summary.cfm" addtoken="no">
</cfif>

<cf_header title="FDA eRotation System - Welcome">

	<header class="services-header" style="border-top: 1px solid #040404; border-bottom: 1px solid #040404">
		<div class="primary-dark-div">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="service-header-text center-block">
							<h1 class="animated fadeInDown animation-delay-2">FDA eRotation</h1>
							<h1 class="animated fadeInDown animation-delay-5">Electronic Rotation Opportunity Management Program</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div>
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					<h2>Government Warning Notice</h2>
					This is a U.S. Government computer system, which may be accessed and used only for authorized Government business by authorized personnel. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action.<br>
					<br>
					All information on this computer system may be intercepted, recorded, read, copied, and disclosed by and to authorized personnel for official purposes, including criminal investigations. Such information includes sensitive data encrypted to comply with confidentiality and privacy requirements. Access or use of this computer system by any person, whether authorized or unauthorized, constitutes consent to these terms. There is no right of privacy in this system.
				</div>
				<div class="col-md-2 pull-right" style="margin-top: 40px">
					<img src="assets/img/fda_logo.png" class="img-responsive"><br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
		</div>
	</div>

<cf_footer>