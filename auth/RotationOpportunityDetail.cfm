<cf_header title="Rotation Opportunity Entry" noNav="true">
<!---Prevent crash if session has timed out--->
    <cfif !structKeyExists(session, "EMPLOYEE_UUID")>
        <h2>Your session appears to have timed out</h2>
        <cfoutput>
            <p>Please log in by going <a href="#application.baseURL#/index.cfm">back to the portal page</a> or to <a href="#application.baseURL#/login.cfm">the eRotation login page.</a></p>
        </cfoutput>
    </cfif>
    <style>
        .grayLabel {
            background-color: #d6dde3;
            color: #030033;
            font-size: 1.6rem;
            vertical-align: text-bottom;
            horiz-align: center;
            width: 15%;
        }
        table, th, td {
            border: 2px solid black;
            padding: 10px;
        }
</style>
    <!---BEGIN: process expression of interest--->
    <cfif structkeyexists(FORM, "btnSubmitForConsideration")>
    <h2>Thank You for Your Expression of Interest!</h2>
    <p>You will receive a copy of your expression of interest by email...</p>

    <cfset variables.insRotationRequests = "">
	<cfquery name="insRotationRequests">
        INSERT INTO RotationRequests
        (
        rotOpp_ID
        ,rotationTitle
        ,hostSupervisorCaphr_ID
        ,participantCapHR_ID
        ,participantName
        ,participantCenter
        ,participantJobTitle
        ,participantPhone
        ,participantEmail
        ,dtParticipantRequested
        ,dtLastAction
        ,isActive
        )
        VALUES
        (
		<cfqueryparam value="#FORM.rotOpp_ID#" cfsqltype="cf_sql_integer">
        ,
		<cfqueryparam value="#FORM.rotationTitle#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.hostSupervisorCaphr_ID#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantCapHR_ID#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantName#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantCenter#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantJobTitle#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantPhone#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#FORM.participantEmail#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#DateFormat(now(), "MM/DD/YYYY")#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value="#DateFormat(now(), "MM/DD/YYYY")#" cfsqltype="cf_sql_varchar">
        ,
		<cfqueryparam value=1 cfsqltype="cf_sql_integer">
        )
	</cfquery>
    <!---allow for testing but change to URL when working?--->
	<cfset testing = false>
	<cfif !testing>
		<cfmail to="#FORM.hostSupervisorEmail#" cc="#FORM.participantEmail#,OO-OHCM-eRotation@fda.hhs.gov" from="OO-OHCM-eRotation@fda.hhs.gov" subject="Interest in an FDA Rotation - Opp ID: #FORM.rotOpp_ID#" type="html">
            The following Employee has expressed interest for the following FDA Rotation:<br>
            <hr class="color">
            <div><strong>Rotation number: </strong>&nbsp;#FORM.rotOpp_ID#</div>
        <div><strong>Center: </strong>&nbsp;#FORM.txtCenterOffice#</div>
        <div><strong>Office/Division/Sub-component: </strong>&nbsp;#FORM.txtSubComponent#</div>
        <div><strong>Host Supervisor: </strong>&nbsp;#FORM.hostSupervisorName#</div>
        <div><strong>Rotation Title: </strong>&nbsp;#FORM.rotationTitle#</div>
        <div><strong>View this Rotation Opportunity at: </strong><a href="#application.baseURL#/auth/RotationOpportunityDetail.cfm?rotOpp_ID=#FORM.rotOpp_ID#" title="Click link or copy/paste to your browser...">#application.baseURL#/RotationOpportunityDetail.cfm?rotOpp_ID=#FORM.rotOpp_ID#</a></div><BR>
        <hr class="color">
        <div><strong>Participant Info:</strong>&nbsp;</div>
        <hr class="color">
        <div><strong>Name:</strong>&nbsp;#FORM.participantName#</div>
        <div><strong>Current Center: </strong>&nbsp;#FORM.participantCenter#"</div>
        <div><strong>Current Rotation Title: </strong>&nbsp;#FORM.participantJobTitle#</div>
        <div><strong>Phone: </strong>&nbsp;#FORM.participantPhone#</div>
        <div><strong>Email: </strong>&nbsp;#FORM.participantEmail#</div>
        <br>
        Please contact the participant to gain additional information required.<br>
        Once the rotation is filled, please return to the FDA Rotational Management platform and update the rotation status.
        <br><br>
        Thanks,
        FDA eRotation Support Team
		</cfmail>
	<cfelse>
<!---TODO: use the developer in system settings for this application--->
        <cfmail to="jdavis@gapsi.com" from="eRotation" subject="Expression of Interest from " type="html">
            Hi There from eRotation, the mail went through!
        </cfmail>
	</cfif>


    <a href="Summary.cfm">Return to Summary view</a>
</cfif>
    <!---END: process expression of interest data--->

    <cfset variables.FORM = []/>
    <!---<cfset variables.qry_getRotationOppByID = {}/>--->
    <!---Use of selects as seen in ePMAP is returning the ID rather than the text
    for centerOffice and subComponent.
    While this is more efficient storage-wise, it requires decoding
    since the list of values table is in another database
    with storing indexes, those two fields can be reduced in size
    --->
    <cfif StructKeyExists(URL, "rotOpp_ID") AND len(url.rotOpp_ID)>
	<cftry>
        <!---<cfset var qry_getRotationOppByID = "">--->
        <cfquery name="qry_getRotationOppByID">
            SELECT
            centerOffice
            ,subComponent
            ,justification
            ,rotationTitle
            ,clearencesRequired
            ,numPositions
            ,officeAddress
            ,execSponsor
            ,hostSupervisorCapHR_ID
            ,hostSupervisorName
            ,hostSupervisorTitle
            ,hostSupervisorEmail
            ,hostSupervisorPhone
            ,hostSupervisorSignature
            ,hostSupervisorSignDate
            ,LDP_PMC_Fellow
            ,agencyPOC
            ,workplaceFlexibilities
            ,newRotationHost
            ,opportunityDescription
            ,ECQ_LeadingChange
            ,ECQ_LeadingPeople
            ,ECQ_ResultsDriven
            ,ECQ_BusinessAcumen
            ,ECQ_BuildingCoalitions
            ,ECQ_HowOppRelates
            ,devOpp1
            ,devOpp2
            ,devOpp3
            ,devOpp4
            ,devOpp5
            ,devOpp6
            ,devOpp7
            ,devOpp8
            ,devOpp9
            ,devOpp10
            ,devOpp11
            ,devOpp12
            ,otherDevOpp
            ,howBenefitParticipant
            ,specialRequirements
            ,dateEntered
            ,directorsCAPHR_ID
            ,directorsSignDate
            ,rotationOppStatus
            ,offeredToCapHR_ID
            ,acceptedByCapHR_ID
            ,isActive
            ,participantLocation
            FROM RotationOpp
            WHERE rotOpp_ID =
			<cfqueryparam value="#URL.rotOpp_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfcatch>
			<cfdump var="#cfcatch#" label="cfcatch" expand="false" abort="false"/>
		</cfcatch>
	</cftry>

    <cfset variables.qry_GetRotationCenter = "">
	<cfquery name="qry_GetRotationCenter">
        Select
        Admin_Code
        ,Center_Name
        ,Center_Acronym
        ,Center_Active
        FROM FDA_Centers
        WHERE Center_ID = <cfqueryparam value="#qry_getRotationOppByID.centerOffice#" cfsqltype="cf_sql_integer" null="#NOT(trim(len(qry_getRotationOppByID.centerOffice)))#">
	</cfquery>

    <cfset variables.qry_GetRotationOffice = "">
	<cfquery name="qry_GetRotationOffice">
        Select
        office_name
        ,office_admin_code
        ,center_id
        FROM FDA_Offices
        WHERE office_id =
        <cfqueryparam value="#qry_getRotationOppByID.subComponent#" cfsqltype="cf_sql_integer" null="#NOT(trim(len(qry_getRotationOppByID.subComponent)))#">
	</cfquery>

<!---Get the participant data not already available in the session--->
	<cfset variables.qry_getEmployeeDataByID = ""/>
	<cfquery name="qry_getEmployeeDataByID">
        SELECT
        name participantName
        ,email_address participantEmail
        ,Phone participantPhone
        ,Job_Title participantJobTitle
        ,CapHR_ID participantCapHR_ID
        ,center participantCenter
        ,Office participantOffice
        FROM    Employees
        WHERE   Employee_UUID =
		<cfqueryparam value="#session.Employee_UUID#" cfsqltype="cf_sql_varchar">
	</cfquery>

<cfelse>
    <h3>Missing which opportunity you were looking for.</h3>
    <a href="Summary.cfm">Return to Summary</a>
	<cfabort>
</cfif>

<cftry>
		<cfinvoke component="CFCs/FDAData" method="getActiveCenters" returnvariable="rtn_getActiveCenters">
		<cfinvoke component="CFCs/FDAData" method="getAllOffices" returnvariable="rtn_getAllOffices">
		<cfinvoke component="CFCs/employee" method="qGetEmployeeData" uuid="#session.employee_uuid#" returnvariable="rtn_qGetEmployeeData">
	<cfcatch>
		<cfif application.environment IS NOT "Development" and application.environment IS NOT "SharedDev">
			<cferror type="exception" exception="any" template="error.cfm">
		<cfelse>
            <h2>An Error occured!</h2>
		</cfif>
	</cfcatch>
</cftry>

<cfif NOT rtn_qGetEmployeeData.RecordCount>
    <br>
    <div class="container">
        <mytags:checkmark type="Warning" Message="Your account not was found" submessage="Please contact the PMAP support team if you need more assistance."/>
    </div>
</cfif>
<div class="container-fluid" style="margin: 0px 25px 0px 25px">
    <div class="row">
        <h2 style="font-weight: 400">FDA&nbsp;Rotation&nbsp;Opportunity&nbsp;Details</h2>
    </div>
<cfif qry_getRotationOppByID.recordcount>
        <div class="row col-md-12 align-items-center">
            <button type="button" class="btn btn-primary" id="btnExpressInterest" data-toggle="modal" data-target="#interestModal" data-whatever="@mdo">
                Express Interest in this Rotation
            </button>
            <a href="Summary.cfm">
                <button type="button" class="btn btn-primary">
                    Return to Summary
                </button>
            </a>
		<cfif isUserInAnyRole("Admin,Developer") OR (isUserInRole("Rating_Official") AND qry_getRotationOppByID.hostSupervisorCapHR_ID EQ session.user_id)>
                <hr class="color"/>
			    <cfoutput>
                <div class="container-fluid">
                    <form action="#application.baseURL#/Auth/RotationOpportunity_Update.cfm?rotOpp_ID=#URL.rotOpp_ID#&action=edit" name="updatRotationStatus" method="post">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="hidden" name="rotOpp_ID" value="#URL.rotOpp_ID#">
                                <select name="rotationOppStatus" id="rotationOppStatus" class="form-control">
                                <option selected="selected" value="0">Update Rotation Status ...</option>
                                <option value="Open">Open</option>
                                <option value="Filled">Filled</option>
                                <option value="Closed">Closed</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 15px">
                            <div class="col-sm-4">
                                <label for="txt_RotationCloseReason">If you close a Rotation for reasons other than filled, please enter the reason:</label>
                                <textarea id="txt_RotationCloseReason" class="form-control" name="txt_RotationCloseReason" placeholder="Reason for closing, if other than all rotation positions filled."></textarea>&nbsp;&nbsp;
                                <button type="submit" id="btn_UpdateRotationStatus" class="btn btn-primary" style="margin-top: 10px">Update Rotation Status</button>
                            </div>
                        </div>
                </form>
                </div>
			    </cfoutput>
            <hr class="color"/>
		</cfif>
        </div>
	<cfoutput query="qry_getRotationOppByID">
        <div class="modal fade" id="interestModal" tabindex="-1" role="dialog" aria-labelledby="interestModalLabel"
             aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="interestModalLabel">Thank you for using the FDA Rotational Management System</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
<form action="#application.baseURL#/auth/RotationOpportunityDetail.cfm?rotOpp_ID=#URL.rotOpp_ID#" method="post">
    <div class="row">
    <div class="col-md-6" style="border-top: 0px white">
        <strong>You have expressed interest for the following rotation:</strong>
    <div class="form-group" style="border-top: 1px solid lightgray">
        <div style="padding-bottom: 10px; padding-top: 15px">
            <strong>Rotation ##:</strong> &nbsp;#URL.rotOpp_ID#
        </div>
        <input type="hidden" name="rotOpp_ID" value="#URL.rotOpp_ID#">
        <div style="padding-bottom: 10px">
        <strong>FDA Center/Office:</strong><br>
        #qry_GetRotationCenter.Center_Acronym# - #qry_GetRotationCenter.Center_Name#
        <input type="hidden" name="txtCenterOffice" value="#qry_GetRotationCenter.Center_Acronym# - #qry_GetRotationCenter.Center_Name#">
            <input type="hidden" name="centerOffice" value="#qry_getRotationOppByID.centerOffice#">
        </div>
        <div style="padding-bottom: 10px">
            <strong>Sub-Office/Division/Sub-Component:</strong><br>
            #qry_GetRotationOffice.office_name#
        </div>
            <input type="hidden" name="txtSubComponent" value="#qry_GetRotationOffice.office_name#">
            <input type="hidden" name="subComponent" value="#qry_getRotationOppByID.subcomponent#">
        <div style="padding-bottom: 10px">
            <strong>Rotational Title:</strong><br>
            #qry_getRotationOppByID.rotationTitle#
        </div>
            <input type="hidden" name="rotationTitle" value="#qry_getRotationOppByID.rotationTitle#">
        </div>
        <div>
            <strong>Rotation Supervisor:</strong><br>#qry_getRotationOppByID.hostSupervisorName#
        </div>
            <input type="hidden" name="hostSupervisorName" value="#qry_getRotationOppByID.hostSupervisorName#">
            <input type="hidden" name="hostSupervisorCaphr_ID" value="#qry_getRotationOppByID.hostSupervisorCaphr_ID#">
            <input type="hidden" name="hostSupervisorEmail" id="hostSupervisorEmail"  value="#qry_getRotationOppByID.hostSupervisorEmail#">
    </div>

    <div class="col-md-6" style="border-top: 0px white">
        <strong>The Rotational Host Manager will be sent the following:</strong>
    <div class="form-group" style="border-top: 1px solid lightgray">

        <div style="padding-bottom: 10px; padding-top: 15px">
            <strong>Name:</strong> &nbsp;#qry_getEmployeeDataByID.participantName#
        </div>
            <input type="hidden" name="participantName" value="#qry_getEmployeeDataByID.participantName#">
            <input type="hidden" name="participantCapHR_ID" value="#qry_getEmployeeDataByID.participantCapHR_ID#">
        <div style="padding-bottom: 10px">
            <strong>My Center:</strong><br>
            #qry_getEmployeeDataByID.participantCenter#"
        </div>
        <input type="hidden" name="participantCenter" value="#qry_getEmployeeDataByID.participantCenter#">
        <div style="padding-bottom: 10px">
            <strong>My Title:</strong><br>
            #qry_getEmployeeDataByID.participantJobTitle#
        </div>
        <input type="hidden" name="participantJobTitle" value="#qry_getEmployeeDataByID.participantJobTitle#">
		<cfoutput>
        <div style="padding-bottom: 10px">
            <label for="participantPhone" class="col-form-label phone">Phone:</label>&nbsp;&nbsp;
            <input type="text" class="form-control" id="participantPhone" name="participantPhone" value="#qry_getEmployeeDataByID.participantPhone#">
        </div>
		</cfoutput>
        <div style="padding-bottom: 10px">
            <label for="participantEmail" class="col-form-label">Email:</label>&nbsp;&nbsp;
            <input type="text" class="form-control" id="participantEmail" name="participantEmail" value="#qry_getEmployeeDataByID.participantEmail#">
        </div>
    </div>
    </div>
    </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btnSubmitForConsideration" name="btnSubmitForConsideration" class="btn btn-primary">Send Application</button>
        </div>
    </form>
    </div>
    </div>
    </div>
    </div><BR>
    <div class="row col-md-12 align-items-md-end">
    <form accept-charset="UTF-8" action="Auth/RotationOpportunity_Save.cfm" autocomplete="off" method="POST">
    <fieldset>
            <input type="hidden" name="hostSupervisorCapHRID" class="form-control" id="hostSupervisorCapHRID"
            value="#session.user_id#"/>
            <input type="hidden" name="hostSupervisorSignDate"
                   id="hostSupervisorSignDate" value="#dateformat(now(), 'yyyy-mm-dd')#"/>
            <input type="hidden" name="dateEntered" id="dateEntered" value="#dateformat(now(), 'yyyy-mm-dd')#"/>
    </fieldset>
    </form>
    </div>
    <style>
    td {
    background-color:  ##ffffff;
    }
    </style>
<!---Display Table for rotation opportunity details--->
    <table style="margin-top: 10px">
        <tr>
        <td colspan="4" class="grayLabel">
                <hr style="height:4px; color:##ff0000; background-color:##ff0000;"/>
        <em>FDA rotation opportunities, particularly those which emphasize cross-Center/Office assignments, provide FDA leaders with quality developmental experiences and tools to achieve both mission and career success. FDA leaders, both participants and hosts, can share their expertise and work
            with others to respond to FDA's mission neeeds and strategic priorities.</em>
    </td>
    </tr>
    <tr>
        <td colspan="4" class="grayLabel"><strong>Instructions:</strong> This form is designed as a resource to assist host supervisors in establishing experiential opportunities that support mission operations. All rotation assignments will be posted internally on the FDA eRotations system. FDA rotations established for the President's Management Council (PMC) Fellowship will be shared externally with HHS and Federal partners.<BR>
                <hr style="height:4px; color:##ff0000; background-color:##ff0000;"/>
        </td>
    </tr>
    <tr align="left">
        <td class="grayLabel">
            <label for="centerOffice">FDA Center/Office:</label>
        </td>
        <td >#qry_GetRotationCenter.Center_Name#</td>
            <td class="grayLabel" colspan="1">
                <label for="subComponent">Office/Division - Sub-component</label>
            </td>
        <td>
            #qry_GetRotationOffice.office_name#
        </td>
    </tr>
    <tr>
        <td colspan="4" class="grayLabel">
            <label for="justification">Specify how this rotation aligns with FDA's strategic goals and supports workforce planning for the Agency:</label>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            #justification#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="rotationTitle">Rotation Title</label>
        </td>
        <td>
            #rotationTitle#
        </td>
        <td class="grayLabel">
            <label for="clearencesRequired">Required Clearances</label>
        </td>
        <td>
            #clearencesRequired#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="numPositions">Number of Positions:</label>
        </td>
        <td align="center">
            #numPositions#
        </td>
            <td class="grayLabel" rowspan="2">
                <label for="officeAddress">Office Address:</label></td>
        <td rowspan="2">
            #officeAddress#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="execSponsor">Executive Sponsor:</label>
        </td>
        <td>
            #execSponsor#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="hostSupervisorName">Host Supervisor Name: </label>
        </td>
        <td>
            #hostSupervisorName#
        </td>
            <td class="grayLabel">
                <label for="participantLocation">Participant Location: </label>
            </td>
        <td>
            #participantLocation#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="hostSupervisorEmail">Host&nbsp;Supervisor Email:</label>
        </td>
        <td>
            #hostSupervisorEmail#
        </td>
            <td class="grayLabel">
                <label for="hostSupervisorTitle">Host Supervisor Title:</label>
            </td>
        <td>
            #hostSupervisorTitle#
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="hostSupervisorPhone">Host&nbsp;Supervisor Phone:</label>
        </td>
        <td>
            #hostSupervisorPhone#
        </td>
            <td class="grayLabel">
                <label for="agencyPOC">Agency POC:</label>
            </td>
        <td>
            #agencyPOC#
        </td>
    </tr>
</table>

    <table>
    <tr>
    <td class="grayLabel" colspan="1">
        <label for="workplaceFlexibilities">Available workplace flexibilities:</label>
    </td>
    <td colspan="3">
    #workplaceFlexibilities#
    </td>
    </tr>
    <tr>
    <td class="grayLabel" colspan="1">
        <label for="newRotationHost">I am a new FA Rotation Host<BR>
            <em style="font-size: smaller">(new to hosting a rotational assignment)</em>
        </label>
    </td>

    <td width="10%">#yesnoformat(newRotationHost)#
    </td>
    </tr>
    <tr>
        <td class="grayLabel" colspan="4">
            <label for="opportunityDescription "><strong>Description of Opportunity: <em>1. Projects, Roles,
                Responsibilities. 2. Anticipated Accomplishments</em></strong></label>
        </td>
    </tr>
    <tr>
    <td colspan="4">
    #opportunityDescription#
    </td>
    </tr>
    <tr>
        <td class="grayLabel" colspan="4">
            <p><strong> Developmental Goals: Please select 2-3 primary Executive Core Qualifications (ECOs) that the
                participant may cultivate on this assignment.</strong> For more information about ECOs, please visit: <a
                    href="https://www.opm.gov/policy-data-oversight/senior-executive-service/executive-core-qualifications/"
                    target="_blank" title="opens external website in a new window.">https://www.opm.gov/policy-data-oversight/senior-executive-service/executive-core-qualifications/</a>
            </p>
        </td>
    </tr>
    <tr>
        <td class="grayLabel" colspan="2">
            <strong><em>
                ECQs (check all that apply)
            </em></strong>
        </td>
        <td class="grayLabel" colspan="2">
            <label for="ECQ_HowOppRelates">Please provide comments about how this assignment relates to the ECQs<BR> and
                will provide a meaningful work experience for the participant:</label>
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="ECQ_LeadingChange">
                Leading Change:</label>
        </td>
        <td>
            <input type="checkbox" name="ECQ_LeadingChange" id="ECQ_LeadingChange" class="form-control" <cfif qry_getRotationOppByID.ECQ_LeadingChange EQ 1>checked</cfif> value="1">
        </td>
        <td rowspan="5" colspan="4">
            <textarea name="ECQ_HowOppRelates" id="ECQ_HowOppRelates" class="form-control" rows="13">#TRIM(qry_getRotationOppByID.ECQ_HowOppRelates)#</textarea>
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="ECQ_LeadingPeople">
                Leading People:</label>
        </td>
        <td>
            <input type="checkbox" name="ECQ_LeadingPeople" id="ECQ_LeadingPeople" class="form-control" <cfif qry_getRotationOppByID.ECQ_LeadingPeople EQ 1>checked</cfif> value="1">
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="ECQ_ResultsDriven">
                Results Driven:</label>
        </td>
        <td >
            <input type="checkbox" name="ECQ_ResultsDriven" id="ECQ_ResultsDriven" class="form-control" <cfif qry_getRotationOppByID.ECQ_ResultsDriven EQ 1>checked</cfif> value="1">
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="ECQ_BusinessAcumen">
                Business Acumen:</label>
        </td>
        <td >
            <input type="checkbox" name="ECQ_BusinessAcumen" id="ECQ_BusinessAcumen" class="form-control" <cfif qry_getRotationOppByID.ECQ_BusinessAcumen EQ 1>checked</cfif> value="1" >
        </td>
    </tr>
    <tr>
        <td class="grayLabel">
            <label for="ECQ_BuildingCoalitions">
                Building Coalitions:</label>
        </td>
        <td >
            <input type="checkbox" name="ECQ_BuildingCoalitions" id="ECQ_BuildingCoalitions" class="form-control" <cfif qry_getRotationOppByID.ECQ_BuildingCoalitions EQ 1>checked</cfif> value="1">
        </td>
    </tr>
    <tr>
        <td class="grayLabel" colspan="4">The Participant will be offered the following developmental opportunities (check all that apply):</td>
    </tr>
    </table>
    <table width="100%">
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp1" name="devOpp1" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp1 EQ 1>checked</cfif>>
            </td>
            <td colspan="7"><label for="devOpp1">A Senior Executive mentor (this may be the host supervisor</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp2" name="devOpp2" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp2 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp2">At least one senior-level shadowing experience </label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp3" name="devOpp3" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp3 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp3">A peer-level work/project advisor</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp4" name="devOpp4" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp4 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp4">Individual Development Plan and regular check-ins on developmental
                    progress </label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp5" name="devOpp5" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp5 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp5">A closing assessment of accomplishments and specific recommendations for continued
                    development </label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp6" name="devOpp6" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp6 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp6">Access and exposure to senior-level meetings</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp7" name="devOpp7" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp7 EQ 7>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp7">Subject-specific onboarding designed to provide learning on a key skill, issue, profession, etc. </label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp8" name="devOpp8" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp8 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp8">Participation in agency-provided training, such as online learning, workshops,
                    speaker series, etc.</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp9" name="devOpp9" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp9 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp9">Supervisory experience </label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp10" name="devOpp10" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp10 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp10">Cross-agency collaboration experience</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp11" name="devOpp11" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp11 EQ 1>checked</cfif>>
            </td>
                <td colspan="7">
                <label for="devOpp11">Project management experience</label>
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="checkbox" id="devOpp12" name="devOpp12" value="1" class="form-control" <cfif qry_getRotationOppByID.devOpp12 EQ 1>checked</cfif>>
            </td>
            <td colspan="1">
                <label for="devOpp12">Other (please explain):</label>
            </td>
                <td colspan="6" bgcolor="##E6F2FB">
        <textarea id="otherDevOpp" name="otherDevOpp" class="form-control"
                  placeholder="text description of other development opportunity">#otherDevOpp#</textarea>
            </td>
        </tr>
    </table>
    <table>
    <tr>
        <td colspan="4" class="grayLabel">
            <label for="otherDevOpp">How would this opportunity benefit the participant and his/her home organization upon their return? </label>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="grayLabel">
    <textarea id="howBenefitParticipant" name="howBenefitParticipant" class="form-control"
    placeholder="text description of how opportunity would benefit the participant">#howBenefitParticipant#</textarea>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="grayLabel">
            <label for="specialRequirements">Special Requirements (if any):</label>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="grayLabel">
            <textarea name="specialRequirements" id="specialRequirements" class="form-control" >#specialRequirements#</textarea>
        </td>
    </tr>
    <tr>
    <td class="grayLabel" width="300rem;">
        <label for="hostSupervisorSignature">Host Supervisor's Signature</label>
    </td>
    <td>#hostSupervisorSignature#</td>
    <td class="grayLabel">
    <label for="hostSupervisorSignDate" class="control-label">Date</label>
    </td>
    <td>
        #dateformat(hostSupervisorSignDate,"MM-DD-YYYY")#
        </td>
    </tr>
    </table>
    </div>
	</cfoutput>
</cfif>
<cf_footer>
