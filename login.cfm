<cf_header Title="FDA eRotation System" noNav="True">

<cfset variables.username = listLast(cgi.auth_user, "\")>

<cfif structKeyExists(session, 'fda_username') AND LEN(trim(session.fda_username))>
	<cfset variables.username = session.fda_username>
<cfelseif (structKeyExists(CGI,'auth_user') AND LEN(trim(CGI.auth_user)))>
	<cfset variables.username = cgi.auth_user>
</cfif>

<!--- This allows me to use Chrome as my development browser, which cannot authenticate against IIS on localhost with a PIV card --->
<cfif application.environment IS "Development">
	<cfif structKeyExists(application,'localUserName') AND LEN(trim(application.localUserName))>
		<cfset variables.username = application.localUserName>
	</cfif>
</cfif>

<!--- This allows us to force login as someone else --->
<cfif isDefined('url.username')>
	<cfif application.environment IS NOT "Development">
		<cfif cgi.auth_user EQ "FDA\David.Jacobson" OR cgi.auth_user EQ "FDA\CHRISTOPHE.CHRONIGER" OR cgi.auth_user EQ "FDA\CHERYL.MONTGOMERY" OR cgi.auth_user EQ "FDA\Jeffrey.Davis" >
			<cfset variables.username = "FDA\#url.username#">
		</cfif>
	<cfelseif application.environment IS "Development">
		<cfset variables.username = "FDA\#url.username#">
	</cfif>
</cfif>

	<cfinvoke component="CFCs/login" method="GetAuthentication" user="#variables.username#" returnvariable="qGetAuthentication">

	<!--- This sets the SECOND variable, indicating there are accounts that are both active and inactive (first query) vs only active accounts (second query, this one)  --->
	<cfset variables.AccountsFound_Active = qGetAuthentication.recordcount>

	<!--- Did we find ONE and ONLY ONE active account?  If so, log them in --->
	<cfif qGetAuthentication.recordcount EQ 1 and qGetAuthentication.IsActive EQ 1>

		<cfinvoke component="CFCs/login" method="InsertLastLogin" userID="#qGetAuthentication.NED_ID#">

		<cfset variables.LastLoggedIn = now()>
		<cfset variables.Role_Name = qGetAuthentication.Role_Name>

		<cfif variables.username EQ "FDA\Jeffrey.Davis" and structKeyExists(url,'adminUUID') and url.adminUUID EQ "CEA15097-E290-27CD-EF4907F0B7176AEB">
			<cfset variables.Role_Name = "Developer">
			<cfelseif variables.username EQ "FDA\David.Jacobson" and structKeyExists(url,'adminUUID') and url.adminUUID EQ "CEA15097-E290-27CD-EF4907F0B7176AEB">
			<cfset variables.Role_Name = "Developer">
		</cfif>

		<cflogin
				allowconcurrent="false"
				usebasicauth="true">

			<cfloginuser
					name="#listFirst(variables.username, ".")# #listLast(variables.username, ".")#"
					password=""
					roles="#variables.Role_Name#">
		</cflogin>

		<cfset session.email_address = qGetAuthentication.email_Address>
		<cfset session.user_id = numberFormat(qGetAuthentication.ned_id, "00000000")>
		<cfset session.Employee_UUID = qGetAuthentication.Employee_UUID>
		<cfset session.username = qGetAuthentication.first_name & ' ' & qGetAuthentication.last_name>
		<cfset session.display_Name = qGetAuthentication.display_Name>
		<cfset session.first_name = qGetAuthentication.first_name>
		<cfset session.last_Name = qGetAuthentication.last_name>
		<cfset session.FDA_username = qGetAuthentication.FDA_Username>
		<cfset session.employee_Roles = variables.Role_Name>
		<cfset session.employee_Center_ID = qGetAuthentication.center_id>
		<cfset session.employee_center_name = qGetAuthentication.Center_name>
		<cfset session.employee_center_acronym = qGetAuthentication.Center_acronym>
		<cfset session.authenticated = 1>
		<cfset session.last_logged_in = variables.LastLoggedIn>
		<cfset session.job_title = qGetAuthentication.job_title>
		<cfset session.usersManager = qGetAuthentication.Supervisor_Full_name>
		<cfset session.usersManagerID = numberFormat(qGetAuthentication.supervisor_ned_id, "00000000")>
		<cfset session.isCommissionedCorps = qGetAuthentication.isCommissionedCorps>
		<cfset session.isExecutive = qGetAuthentication.isExecutive>

		<cfif trim(qGetAuthentication.display_name) NEQ "">
			<cfset session.display_name = qGetAuthentication.display_name>
		<cfelse>
			<cfset session.display_name = session.username>
		</cfif>

		<header class="services-header">
			<div class="primary-dark-div">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="service-header-text center-block">
								<h1 class="animated fadeInDown animation-delay-2">Logging in ... </h1>
								<h1 class="animated fadeInDown animation-delay-5">Loading all Rotation Opportunity Records . . .</h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<cfoutput>
				<meta http-equiv="REFRESH" content="1; URL='auth/summary.cfm'">
			</cfoutput>

	<cfelseif qGetAuthentication.recordcount EQ 1 and qGetAuthentication.IsActive EQ 0>

		<div class="container">
			<div class="text-center" style="margin-top: 25px">
				<cf_checkmark type="Warning" message="Your account is not active." submessage="Please contact your Supervisor for more information.">
				<cfoutput>
				<a href="mailto:#request.supportEmail#"><i class="fa fa-envelope"></i> Click here</a> to email eRotation support.
				</cfoutput>
			</div>
			<div class="text-center" style="margin-top: 50px">
				<cfoutput>
				<button type="button" class="btn btn-info" onclick="window.location.href='#application.BaseURL#/'">Back to Login</button>
				</cfoutput>
			</div>
		</div>

	<cfelse>

		<div class="container" style="margin-top: 50px; margin-bottom: 50px">
			<div class="text-center">
				<cf_checkmark type="Warning" message="Your are not authorized to access this application." submessage="Please contact your Supervisor for more information.">
				<cfoutput>
				<i class="fa fa-envelope" style="margin-right: 5px; color: ##0099da"></i><a href="mailto:#request.supportEmail#" style="text-decoration: underline">Click here</a> to email the eRotation support.
				</cfoutput>
			</div>
			<div class="text-center" style="margin-top: 50px">
				<cfoutput>
				<button type="button" class="btn btn-info" onclick="window.location.href='#application.BaseURL#/'">Back to Login</button>
				</cfoutput>
			</div>
		</div>

	</cfif>

<cf_footer>
