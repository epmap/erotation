<cfif structKeyExists(FORM, "fieldnames")>
    <h2>Form variables detected!</h2>
	<!---TODO: Update that rotopp_ID with txt_RotationCloseReason and rotationOppStatus--->
	<cfdump var="#FORM#" label="FORM" expand="true" abort="false"/>
	<cftry>
		<cfcatch type="any">
			<cfdump var="#cfcatch#" label="Cfcatch" expand="true" abort="true"/>
		</cfcatch>
	</cftry>
    <cfif structKeyExists(FORM, "ROTATIONOPPSTATUS")>
		<cfset variables.qry_updateRotationStatus = "">
		<cfquery name="qry_updateRotationStatus" >
        UPDATE 	RotationOpp
		SET 	txt_RotationCloseReason = <cfqueryparam value="#FORM.txt_RotationCloseReason#" cfsqltype="cf_sql_longvarchar">,
				rotationOppStatus = <cfqueryparam value="#FORM.rotationOppStatus#" cfsqltype="cf_sql_varchar">,
				<cfif FORM.rotationOppStatus NEQ 'Open'>
				isActive = <cfqueryparam value="0" cfsqltype="cf_sql_varchar">
				<cfelse>
				isActive = <cfqueryparam value="1" cfsqltype="cf_sql_varchar">
				</cfif>
		WHERE 	rotOpp_ID = <cfqueryparam value="#FORM.rotOpp_ID#">
        </cfquery>

    </cfif>
	<cflocation url="RotationOpportunityDetail.cfm?rotOpp_ID=#FORM.rotOpp_ID#&updateSuccess"/>
</cfif>